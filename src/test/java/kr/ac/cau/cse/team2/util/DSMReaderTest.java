package kr.ac.cau.cse.team2.util;

import com.google.common.collect.Table;
import kr.ac.cau.cse.team2.util.io.DSMReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Test for DSMReader
 */
public class DSMReaderTest {
    private static final Boolean O = Boolean.TRUE;
    private static final Boolean X = Boolean.FALSE;

    @Test
    public void testRead() throws DSMReader.InvalidDSMFormatException, IOException {
        String input = "4\n" +
                "0 1 0 0\n" +
                "1 0 0 1\n" +
                "0 1 0 0\n" +
                "1 0 0 0\n" +
                "Foo\n" +
                "Bar\n" +
                "Baz\n" +
                "Qux";
        DSMReader io = new DSMReader();
        Pair<Table<Integer, Integer, Boolean>, List<String>> pair = io.readDSMFromString(input);

        Table<Integer, Integer, Boolean> matrix = pair.getLeft();
        List<String> names = pair.getRight();

        assert 4 == matrix.rowKeySet().size();
        assert 4 == matrix.columnKeySet().size();
        assert 4 == names.size();

        Assert.assertEquals("Foo", names.get(0));
        Assert.assertEquals("Bar", names.get(1));
        Assert.assertEquals("Baz", names.get(2));
        Assert.assertEquals("Qux", names.get(3));

        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O, X, X},
                {O, X, X, O},
                {X, O, X, X},
                {O, X, X, X}
        });
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testCRLFRead() throws DSMReader.InvalidDSMFormatException, IOException {
        String input = "4\n" +
                "0 1 0 0\n" +
                "1 0 0 1\n" +
                "0 1 0 0\n" +
                "1 0 0 0\n" +
                "Foo \r\n" +
                "Bar\r\n" +
                "Baz\r\n" +
                "Qux";
        DSMReader io = new DSMReader();
        Pair<Table<Integer, Integer, Boolean>, List<String>> pair = io.readDSMFromString(input);

        Table<Integer, Integer, Boolean> matrix = pair.getLeft();
        List<String> names = pair.getRight();

        assert 4 == matrix.rowKeySet().size();
        assert 4 == matrix.columnKeySet().size();
        assert 4 == names.size();

        Assert.assertEquals("Foo ", names.get(0));
        Assert.assertEquals("Bar", names.get(1));
        Assert.assertEquals("Baz", names.get(2));
        Assert.assertEquals("Qux", names.get(3));

        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O, X, X},
                {O, X, X, O},
                {X, O, X, X},
                {O, X, X, X}
        });
        Assert.assertEquals(expected, matrix);
    }
}
