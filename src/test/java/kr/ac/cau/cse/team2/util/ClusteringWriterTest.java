package kr.ac.cau.cse.team2.util;

import com.google.common.collect.Lists;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import kr.ac.cau.cse.team2.util.io.ClusteringReader;
import kr.ac.cau.cse.team2.util.io.ClusteringWriter;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hardtack on 2014. 6. 1..
 */
public class ClusteringWriterTest {
    @Test
    public void testClusteringWriter() throws IOException, ClusteringReader.InvalidClusteringFormatException {

        Module moduleA = new Module("A");
        Module moduleB = new Module("B");
        Module moduleC = new Module("C");
        Module moduleD = new Module("D");
        Module moduleE = new Module("E");
        Module moduleF = new Module("F");
        Module moduleG = new Module("G");

        GroupedModule expected = new GroupedModule("$root");
        GroupedModule group1 = new GroupedModule("g1", moduleB, moduleC);
        GroupedModule group2 = new GroupedModule("g2", moduleD, moduleE, moduleF);
        expected.addSubModule(moduleA);
        expected.addSubModule(group1);
        expected.addSubModule(group2);
        expected.addSubModule(moduleG);

        ClusteringWriter writer = new ClusteringWriter();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            writer.writeClustering(Arrays.asList((Module) expected), stream);
        } finally {
            stream.close();
        }
        String data = new String(stream.toByteArray());

        List<Module> origins = Lists.newArrayList(moduleA, moduleB, moduleC, moduleD, moduleE, moduleF, moduleG);

        List<Module> modules;
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes());
        try {
            modules = new ClusteringReader().readClustering(inputStream, origins);
        } finally {
            inputStream.close();
        }

        assert 1 == modules.size();
        Assert.assertEquals(expected, modules.get(0));
    }
}
