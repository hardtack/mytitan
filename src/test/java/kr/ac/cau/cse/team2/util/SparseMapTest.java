package kr.ac.cau.cse.team2.util;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Test for sparse map
 */
public class SparseMapTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testSparseMap() {
        SparseMap<Integer> map = new SparseMap<Integer>(3, 0);
        map.put(0, 1);
        map.put(2, 2);

        assert 1 == map.get(0);
        assert 0 == map.get(1);
        assert 2 == map.get(2);

        Assert.assertEquals(Sets.newHashSet(0, 1, 2), map.keySet());
        Assert.assertEquals(Arrays.asList(1, 0, 2), new ArrayList<Integer>(map.values()));

        exception.expect(IndexOutOfBoundsException.class);
        map.get(3);

        exception.expect(IndexOutOfBoundsException.class);
        map.put(3, 3);
    }
}
