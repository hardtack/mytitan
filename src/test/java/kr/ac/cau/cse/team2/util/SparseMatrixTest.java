package kr.ac.cau.cse.team2.util;

import com.google.common.collect.Table;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test for sparse matrix
 */
public class SparseMatrixTest {
    @Test
    public void testSparseMatrix() {
        SparseMatrix<Integer> matrix = new SparseMatrix<Integer>(2, 3, 0);
        matrix.put(0, 0, 1);
        matrix.put(1, 0, 2);
        matrix.put(1, 1, 3);

        // We have matrix like following
        // +---+---+---+
        // | 1 | O | 0 |
        // +---+---+---+
        // | 2 | 3 | O |
        // +---+---+---+
        //
        assert 1 == matrix.get(0, 0);
        assert 0 == matrix.get(0, 1);
        assert 0 == matrix.get(0, 2);
        assert 2 == matrix.get(1, 0);
        assert 3 == matrix.get(1, 1);
        assert 0 == matrix.get(1, 2);

        Table<Integer, Integer, Integer> arrayMatrix = MatrixUtil.arrayToMatrix(new Integer[][]{
                {1, 0, 0},
                {2, 3, 0}
        });
        Assert.assertEquals(arrayMatrix, matrix);
        Assert.assertEquals(arrayMatrix.cellSet(), matrix.cellSet());
        for (int i = 0; i < 3; i++) {
            Assert.assertEquals(arrayMatrix.column(i), matrix.column(i));
        }
        for (int i = 0; i < 2; i++) {
            Assert.assertEquals(arrayMatrix.row(i), matrix.row(i));
        }
    }
}
