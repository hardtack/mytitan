package kr.ac.cau.cse.team2.model;

import com.google.common.collect.Lists;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import kr.ac.cau.cse.team2.model.operation.ModuleTraversable;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test module traverse
 */
public class ModuleTraverseTest {
    @Test
    public void testModuleTraverse() {
        GroupedModule root = new GroupedModule("root", true);

        Module foo = new Module("foo");
        Module bar = new Module("bar");
        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(foo, bar, baz));

        List<Module> expected = Arrays.asList(foo, bar, baz);
        List<Module> actual = Lists.newLinkedList(new ModuleTraversable(root));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testModuleTraverseDeep() {
        GroupedModule root = new GroupedModule("root", true);

        GroupedModule group = new GroupedModule("group", true);
        Module foo = new Module("foo");
        group.addSubModule(foo);
        Module bar = new Module("bar");
        group.addSubModule(bar);

        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(group, baz));

        List<Module> expected = Arrays.asList(foo, bar, baz);
        List<Module> actual = Lists.newLinkedList(new ModuleTraversable(root));
        Assert.assertEquals(expected, actual);

        // Close and test again
        group.setOpen(false);
        expected = Arrays.asList(group, baz);
        actual = Lists.newLinkedList(new ModuleTraversable(root));
        Assert.assertEquals(expected, actual);
    }
}
