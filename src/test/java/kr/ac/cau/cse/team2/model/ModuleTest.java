package kr.ac.cau.cse.team2.model;

import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test for modules
 */
public class ModuleTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testModule() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");

        // Check name
        Assert.assertEquals("Foo", foo.getName());
        Assert.assertEquals("Bar", bar.getName());

        // Add dependency
        foo.addDependency(bar);  // Foo has dependency on Bar

        // Check
        assert foo.hasDependencyOn(bar);
        assert !bar.hasDependencyOn(foo);

        // Remove dependency
        foo.removeDependency(bar);
        assert !foo.hasDependencyOn(bar);
        assert !bar.hasDependencyOn(foo);
    }

    @Test
    public void testGroupedModule() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");

        GroupedModule group = new GroupedModule("Group", bar, baz);

        foo.addDependency(baz);  // Foo has dependency on Baz
        assert foo.hasDependencyOn(group);

        bar.addDependency(foo);  // Bar has dependency on Foo
        assert group.hasDependencyOn(foo);
    }

    @Test
    public void testTwoGroupedModule() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");
        Module qux = new Module("Qux");

        GroupedModule group1 = new GroupedModule("Group1", foo, bar);
        GroupedModule group2 = new GroupedModule("Group1", baz, qux);

        foo.addDependency(qux);  // Foo has dependency on Qux
        assert group1.hasDependencyOn(group2);
        assert !group2.hasDependencyOn(group1);

        baz.addDependency(bar);  // Baz has dependency on Bar
        assert group2.hasDependencyOn(group1);
        assert group1.hasDependencyOn(group2);
    }

    @Test
    public void testNestedGroupedModule() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");
        Module qux = new Module("Qux");

        GroupedModule innerGroup = new GroupedModule("Inner Group", baz, qux);
        GroupedModule group = new GroupedModule("Inner Group", bar, innerGroup);

        foo.addDependency(baz);  // Foo has dependency on Baz
        assert foo.hasDependencyOn(innerGroup);
        assert foo.hasDependencyOn(group);
        assert !innerGroup.hasDependencyOn(foo);
        assert !group.hasDependencyOn(foo);

        qux.addDependency(foo);  // Qux has dependency on Foo
        assert innerGroup.hasDependencyOn(foo);
        assert group.hasDependencyOn(foo);
        assert foo.hasDependencyOn(innerGroup);
        assert foo.hasDependencyOn(group);
    }

    @Test
    public void testContaining() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        GroupedModule group = new GroupedModule("Group", bar);

        assert !group.containsModule(foo);
        assert group.containsModule(bar);
    }

    @Test
    public void testNestedContaining() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");
        GroupedModule group1 = new GroupedModule("Group1", baz);
        GroupedModule group2 = new GroupedModule("Group2", bar, group1);

        assert !group1.containsModule(foo);
        assert !group1.containsModule(bar);
        assert group1.containsModule(baz);
        assert !group1.containsModule(group2);

        assert !group1.containsModule(foo);
        assert group2.containsModule(bar);
        assert group2.containsModule(baz);
        assert group2.containsModule(group1);
    }

    @Test
    public void testGroupedModuleCannotHaveDependency() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");

        GroupedModule group = new GroupedModule("Group", bar, baz);

        // We cannot add dependency to group directly
        exception.expect(UnsupportedOperationException.class);
        foo.addDependency(group);

        // Also as converted
        exception.expect(UnsupportedOperationException.class);
        group.addDependency(foo);
    }
}
