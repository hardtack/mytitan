package kr.ac.cau.cse.team2.util;

import com.google.common.collect.Lists;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import kr.ac.cau.cse.team2.util.io.ClusteringReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by hardtack on 2014. 6. 1..
 */
public class ClusteringReaderTest {
    @Test
    public void testClusteringReader() throws IOException, ClusteringReader.InvalidClusteringFormatException {
        String input = "<cluster>\n" +
                "<group name=\"$root\">\n" +
                "  <item name=\"A\" />\n" +
                "  <group name=\"g1\">\n" +
                "    <item name=\"B\" />\n" +
                "    <item name=\"C\" />\n" +
                "  </group>\n" +
                "  <group name=\"g2\">\n" +
                "    <item name=\"D\" />\n" +
                "    <item name=\"E\" />\n" +
                "    <item name=\"F\" />\n" +
                "  </group>\n" +
                "  <item name=\"G\" />\n" +
                "</group>\n" +
                "</cluster>";
        List<Module> modules;

        Module moduleA = new Module("A");
        Module moduleB = new Module("B");
        Module moduleC = new Module("C");
        Module moduleD = new Module("D");
        Module moduleE = new Module("E");
        Module moduleF = new Module("F");
        Module moduleG = new Module("G");

        // Pre-defined dependency
        moduleA.addDependency(moduleG);

        List<Module> origins = Lists.newArrayList(moduleA, moduleB, moduleC, moduleD, moduleE, moduleF, moduleG);

        ByteArrayInputStream stream = new ByteArrayInputStream(input.getBytes());
        try {
            modules = new ClusteringReader().readClustering(stream, origins);
        } finally {
            stream.close();
        }

        GroupedModule expected = new GroupedModule("$root");
        GroupedModule group1 = new GroupedModule("g1", moduleB, moduleC);
        GroupedModule group2 = new GroupedModule("g2", moduleD, moduleE, moduleF);
        expected.addSubModule(moduleA);
        expected.addSubModule(group1);
        expected.addSubModule(group2);
        expected.addSubModule(moduleG);

        assert 1 == modules.size();
        Assert.assertEquals(expected, modules.get(0));
        assert expected.getSubModules().get(0).hasDependencyOn(expected.getSubModules().get(expected.getSubModules().size() - 1));
    }
}
