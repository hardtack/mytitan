package kr.ac.cau.cse.team2.model;

import com.google.common.collect.Table;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import kr.ac.cau.cse.team2.model.operation.DSM;
import kr.ac.cau.cse.team2.util.MatrixUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * DSM Test
 */
public class DSMTest {
    private static final Boolean O = Boolean.TRUE;
    private static final Boolean X = Boolean.FALSE;

    @Test
    public void testEmptyDependency() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        List<Module> modules = Arrays.asList(foo, bar);
        Table<Integer, Integer, Boolean> matrix = DSM.createDSM(modules);

        // Dependency checking
        // We must have matrix like following
        // +-----+-----+-----+
        // |     | Foo | Bar |
        // +=====+=====+=====+
        // | Foo |     |     |
        // +-----+-----+-----+
        // | Bar |     |     |
        // +-----+-----+-----+
        //
        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, X},
                {X, X},
        });
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testSingleDependency() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        List<Module> modules = Arrays.asList(foo, bar);
        // Add dependency (foo depends on bar)
        foo.addDependency(bar);
        Table<Integer, Integer, Boolean> matrix = DSM.createDSM(modules);
        // We must have matrix like following
        // +-----+-----+-----+
        // |     | Foo | Bar |
        // +=====+=====+=====+
        // | Foo |     |  O  |
        // +-----+-----+-----+
        // | Bar |     |     |
        // +-----+-----+-----+
        //
        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O},
                {X, X},
        });
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testTwinDependency() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        List<Module> modules = Arrays.asList(foo, bar);
        // Add dependency (bar depends on foo)
        foo.addDependency(bar);
        bar.addDependency(foo);
        Table<Integer, Integer, Boolean> matrix = DSM.createDSM(modules);
        // We must have matrix like following
        // +-----+-----+-----+
        // |     | Foo | Bar |
        // +=====+=====+=====+
        // | Foo |     |  O  |
        // +-----+-----+-----+
        // | Bar |  O  |     |
        // +-----+-----+-----+
        //
        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O},
                {O, X},
        });
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testFromModuleToGroup() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");

        GroupedModule group = new GroupedModule("Group", Arrays.asList(bar, baz), false);

        // Add some dependencies..
        foo.addDependency(bar);  // Foo has dependency on the Bar
        bar.addDependency(baz);  // Bar has dependency on the Baz

        // Create DSM
        List<Module> modules = Arrays.asList(foo, group);
        Table<Integer, Integer, Boolean> matrix = DSM.createDSM(modules);

        // Since group is closed, the size of matrix is 2 by::
        // 0 : Foo
        // 1 : Group
        assert !group.isOpen();

        // Check grouped dependency
        // We must have matrix like following
        // +-------+-----+-------+
        // |       | Foo | Group |
        // +=======+=====+=======+
        // |  Foo  |     |   O   |
        // +-------+-----+-------+
        // | Group |     |       |
        // +-------+-----+-------+
        //
        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O},
                {X, X},
        });
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testFromGroupToModule() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");

        GroupedModule group = new GroupedModule("Group", Arrays.asList(bar, baz), false);

        // Add some dependencies..
        baz.addDependency(foo);  // Baz has dependency on the Foo

        // Add dependency from Group to Foo
        List<Module> modules = Arrays.asList(foo, group);
        Table<Integer, Integer, Boolean> matrix = DSM.createDSM(modules);

        // Now, we must have matrix like following
        // +-------+-----+-------+
        // |       | Foo | Group |
        // +=======+=====+=======+
        // |  Foo  |     |       |
        // +-------+-----+-------+
        // | Group |  O  |       |
        // +-------+-----+-------+
        //
        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, X},
                {O, X},
        });
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testOpenedGroup() {
        Module foo = new Module("Foo");
        Module bar = new Module("Bar");
        Module baz = new Module("Baz");

        GroupedModule group = new GroupedModule("Group", Arrays.asList(bar, baz), false);

        // Add dependencies
        foo.addDependency(bar);
        bar.addDependency(baz);
        baz.addDependency(foo);

        // When we open the group, the size of matrix increases by::
        // 0 : Foo
        // 1 : Bar
        // 2 : Baz
        group.setOpen(true);
        assert group.isOpen();

        // Create DSM
        List<Module> modules = Arrays.asList(foo, group);
        Table<Integer, Integer, Boolean> matrix = DSM.createDSM(modules);

        // Check opened group dependency
        // We must have matrix like following
        // +-----+-----+-----+-----+
        // |     | Foo | Bar | Baz |
        // +=====+=====+=====+=====+
        // | Foo |     |  O  |     |
        // +-----+-----+-----+-----+
        // | Bar |     |     |  O  |
        // +-----+-----+-----+-----+
        // | Baz |  O  |     |     |
        // +-----+-----+-----+-----+
        //
        Table<Integer, Integer, Boolean> expected = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O, X},
                {X, X, O},
                {O, X, X},
        });
        Assert.assertEquals(expected, matrix);
    }


    @Test
    public void testReverse() {
        // We have matrix like following
        // +-----+-----+-----+-----+
        // |     | Foo | Bar | Baz |
        // +=====+=====+=====+=====+
        // | Foo |     |  O  |     |
        // +-----+-----+-----+-----+
        // | Bar |     |     |  O  |
        // +-----+-----+-----+-----+
        // | Baz |  O  |     |     |
        // +-----+-----+-----+-----+
        //
        Table<Integer, Integer, Boolean> matrix = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O, X},
                {X, X, O},
                {O, X, X},
        });
        List<String> names = Arrays.asList("Foo", "Bar", "Baz");

        List<Module> modules = DSM.createModules(matrix, names);

        // Check size
        assert 3 == modules.size();
        Module foo = modules.get(0);
        Module bar = modules.get(1);
        Module baz = modules.get(2);

        // Check name
        Assert.assertEquals("Foo", foo.getName());
        Assert.assertEquals("Bar", bar.getName());
        Assert.assertEquals("Baz", baz.getName());

        // Check dependency
        assert foo.hasDependencyOn(bar);
        assert !foo.hasDependencyOn(baz);

        assert !bar.hasDependencyOn(foo);
        assert bar.hasDependencyOn(baz);

        assert baz.hasDependencyOn(foo);
        assert !baz.hasDependencyOn(bar);
    }

    @Test
    public void testIndex() {
        GroupedModule root = new GroupedModule("root", true);

        Module foo = new Module("foo");
        Module bar = new Module("bar");
        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(foo, bar, baz));

        assert -1 == DSM.indexOfModule(root, root);
        assert 0 == DSM.indexOfModule(foo, root);
        assert 1 == DSM.indexOfModule(bar, root);
        assert 2 == DSM.indexOfModule(baz, root);
    }

    @Test
    public void testDeepIndex() {
        GroupedModule root = new GroupedModule("root", true);

        GroupedModule group = new GroupedModule("group", true);
        Module foo = new Module("foo");
        group.addSubModule(foo);

        Module bar = new Module("bar");
        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(group, bar, baz));

        assert -1 == DSM.indexOfModule(root, root);
        assert -1 == DSM.indexOfModule(group, root);
        assert 0 == DSM.indexOfModule(foo, root);
        assert 1 == DSM.indexOfModule(bar, root);
        assert 2 == DSM.indexOfModule(baz, root);

        // Close and test again
        group.setOpen(false);
        assert -1 == DSM.indexOfModule(root, root);
        assert 0 == DSM.indexOfModule(group, root);
        assert -1 == DSM.indexOfModule(foo, root);
        assert 1 == DSM.indexOfModule(bar, root);
        assert 2 == DSM.indexOfModule(baz, root);
    }

    @Test
    public void testFindModule() {
        GroupedModule root = new GroupedModule("root", true);

        Module foo = new Module("foo");
        Module bar = new Module("bar");
        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(foo, bar, baz));

        assert foo == DSM.moduleAtIndex(0, root);
        assert bar == DSM.moduleAtIndex(1, root);
        assert baz == DSM.moduleAtIndex(2, root);
    }

    @Test
    public void testFindModuleDeep() {
        GroupedModule root = new GroupedModule("root", true);

        GroupedModule group = new GroupedModule("group", true);
        Module foo = new Module("foo");
        group.addSubModule(foo);

        Module bar = new Module("bar");
        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(group, bar, baz));

        assert foo == DSM.moduleAtIndex(0, root);
        assert bar == DSM.moduleAtIndex(1, root);
        assert baz == DSM.moduleAtIndex(2, root);

        // Close and test again
        group.setOpen(false);
        assert group == DSM.moduleAtIndex(0, root);
        assert bar == DSM.moduleAtIndex(1, root);
        assert baz == DSM.moduleAtIndex(2, root);
        assert null == DSM.moduleAtIndex(3, root);
    }


    @Test
    public void testDSMSize() {
        GroupedModule root = new GroupedModule("root", true);

        Module foo = new Module("foo");
        Module bar = new Module("bar");
        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(foo, bar, baz));

        assert 3 == DSM.dsmSize(root);
    }

    @Test
    public void testDSMSizeDeep() {
        GroupedModule root = new GroupedModule("root", true);

        GroupedModule group = new GroupedModule("group", true);
        Module foo = new Module("foo");
        group.addSubModule(foo);
        Module bar = new Module("bar");
        group.addSubModule(bar);

        Module baz = new Module("baz");

        root.setSubModules(Arrays.asList(group, baz));

        assert 3 == DSM.dsmSize(root);

        // Close and test again
        group.setOpen(false);
        assert 2 == DSM.dsmSize(root);
    }
}
