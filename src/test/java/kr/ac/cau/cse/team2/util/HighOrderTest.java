package kr.ac.cau.cse.team2.util;

import com.google.common.base.Function;
import kr.ac.cau.cse.team2.util.function.HighOrder;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test for high order functions
 */
public class HighOrderTest {
    @Test
    public void testMap() {
        Iterable<String> names = Arrays.asList("Foo", "Bar", "Baz");

        final SideEffectChecker<Boolean> checker = new SideEffectChecker<Boolean>(false);
        List<String> hellos = HighOrder.map(names, new Function<String, String>() {
            @Override
            public String apply(String input) {
                checker.object = true;
                return "Hello, " + input;
            }
        });

        Assert.assertTrue(checker.object);
        assert 3 == hellos.size();
        Assert.assertEquals("Hello, Foo", hellos.get(0));
        Assert.assertEquals("Hello, Bar", hellos.get(1));
        Assert.assertEquals("Hello, Baz", hellos.get(2));
    }

    @Test
    public void testForEach() {
        final List<String> names = Arrays.asList("Foo", "Bar", "Baz");
        final SideEffectChecker<Integer> index = new SideEffectChecker<Integer>(0);
        HighOrder.foreach(names, new HighOrder.ForEach<String>() {
            public void apply(int i, String name) {
                Assert.assertEquals(index.object, new Integer(i));
                Assert.assertEquals(names.get(i), name);
                index.object++;
            }
        });
        Assert.assertEquals(new Integer(3), index.object);
    }

    @Test
    public void testForEachMap() {
        final List<String> names = Arrays.asList("Foo", "Bar", "Baz");
        final SideEffectChecker<Integer> index = new SideEffectChecker<Integer>(0);
        List<String> hellos = HighOrder.foreachMap(names, new HighOrder.ForEachMap<String, String>() {
            public String apply(int i, String name) {
                Assert.assertEquals(index.object, new Integer(i));
                Assert.assertEquals(names.get(i), name);
                index.object++;
                return "Hello, " + name;
            }
        });
        Assert.assertEquals(new Integer(3), index.object);

        assert 3 == hellos.size();
        Assert.assertEquals("Hello, Foo", hellos.get(0));
        Assert.assertEquals("Hello, Bar", hellos.get(1));
        Assert.assertEquals("Hello, Baz", hellos.get(2));
    }

    class SideEffectChecker<T> {
        public T object;

        SideEffectChecker(T object) {
            this.object = object;
        }
    }

}
