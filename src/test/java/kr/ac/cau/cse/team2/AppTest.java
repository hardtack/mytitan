package kr.ac.cau.cse.team2;

import junit.framework.Test;
import junit.framework.TestSuite;
import kr.ac.cau.cse.team2.model.DSMTest;
import kr.ac.cau.cse.team2.model.ModuleTest;
import kr.ac.cau.cse.team2.model.ModuleTraverseTest;
import kr.ac.cau.cse.team2.util.ClusteringReaderTest;
import kr.ac.cau.cse.team2.util.ClusteringWriterTest;
import kr.ac.cau.cse.team2.util.DSMReaderTest;
import kr.ac.cau.cse.team2.util.DSMWriterTest;
import kr.ac.cau.cse.team2.util.HighOrderTest;
import kr.ac.cau.cse.team2.util.MatrixUtilTest;
import kr.ac.cau.cse.team2.util.SparseMapTest;
import kr.ac.cau.cse.team2.util.SparseMatrixTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Unit test for MyTitan.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        DSMTest.class, ModuleTest.class, MatrixUtilTest.class, DSMReaderTest.class, DSMWriterTest.class,
        ClusteringReaderTest.class, ClusteringWriterTest.class, HighOrderTest.class, ModuleTraverseTest.class,
        SparseMapTest.class, SparseMatrixTest.class
})
public class AppTest {

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }
}
