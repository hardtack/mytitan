package kr.ac.cau.cse.team2.util;

import com.google.common.collect.Table;
import kr.ac.cau.cse.team2.util.io.DSMReader;
import kr.ac.cau.cse.team2.util.io.DSMWriter;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Test for DSMWriter
 */
public class DSMWriterTest {
    private static final Boolean O = Boolean.TRUE;
    private static final Boolean X = Boolean.FALSE;

    @Test
    public void testRead() throws DSMReader.InvalidDSMFormatException, IOException {
        Table<Integer, Integer, Boolean> matrix = MatrixUtil.arrayToMatrix(new Boolean[][]{
                {X, O, X, X},
                {O, X, X, O},
                {X, O, X, X},
                {O, X, X, X}
        });
        List<String> names = Arrays.asList("Foo", "Bar", "Baz", "Qux");

        DSMWriter writer = new DSMWriter();
        String data;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            writer.writeDSM(matrix, names, stream);
        } finally {
            stream.close();
        }
        data = new String(stream.toByteArray());

        DSMReader reader = new DSMReader();
        Pair<Table<Integer, Integer, Boolean>, List<String>> pair = reader.readDSMFromString(data);

        Assert.assertEquals(matrix, pair.getLeft());
        Assert.assertEquals(names, pair.getRight());

    }
}
