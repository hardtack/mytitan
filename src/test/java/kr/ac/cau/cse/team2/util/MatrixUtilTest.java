package kr.ac.cau.cse.team2.util;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test for MatrixUtil
 */
public class MatrixUtilTest {
    @Test
    public void testArrayToMatrixBasic() {
        Table<Integer, Integer, Integer> matrix = MatrixUtil.arrayToMatrix(new Integer[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        });
        Table<Integer, Integer, Integer> expected = new ImmutableTable.Builder<Integer, Integer, Integer>()
                .put(0, 0, 1)
                .put(0, 1, 2)
                .put(0, 2, 3)

                .put(1, 0, 4)
                .put(1, 1, 5)
                .put(1, 2, 6)

                .put(2, 0, 7)
                .put(2, 1, 8)
                .put(2, 2, 9)

                .build();
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testArrayToMatrixWithoutFill() {
        Table<Integer, Integer, Integer> matrix = MatrixUtil.arrayToMatrix(new Integer[][]{
                {1, 2, 3},
                {4, 5},
                {6, 7, 8},
        });
        Table<Integer, Integer, Integer> expected = new ImmutableTable.Builder<Integer, Integer, Integer>()
                .put(0, 0, 1)
                .put(0, 1, 2)

                .put(1, 0, 4)
                .put(1, 1, 5)

                .put(2, 0, 6)
                .put(2, 1, 7)

                .build();
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testArrayToMatrixWithFill() {
        Table<Integer, Integer, Integer> matrix = MatrixUtil.arrayToMatrix(new Integer[][]{
                {1, 2, 3},
                {4, 5},
                {6, 7, 8},
        }, 9);
        Table<Integer, Integer, Integer> expected = new ImmutableTable.Builder<Integer, Integer, Integer>()
                .put(0, 0, 1)
                .put(0, 1, 2)
                .put(0, 2, 3)

                .put(1, 0, 4)
                .put(1, 1, 5)
                .put(1, 2, 9)

                .put(2, 0, 6)
                .put(2, 1, 7)
                .put(2, 2, 8)

                .build();
        Assert.assertEquals(expected, matrix);
    }

    @Test
    public void testArrayToMatrixWithSize() {
        Table<Integer, Integer, Integer> matrix = MatrixUtil.arrayToMatrix(new Integer[][]{
                {1, 2, 3},
                {4, 5},
                {6, 7, 8},
        }, 2, 3, 9);
        Table<Integer, Integer, Integer> expected = new ImmutableTable.Builder<Integer, Integer, Integer>()
                .put(0, 0, 1)
                .put(0, 1, 2)
                .put(0, 2, 3)

                .put(1, 0, 4)
                .put(1, 1, 5)
                .put(1, 2, 9)

                .build();
        Assert.assertEquals(expected, matrix);
    }
}
