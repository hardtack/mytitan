package kr.ac.cau.cse.team2.controller.abs;

import java.util.ArrayList;
import java.util.List;

/**
 * Declarative base controller class
 *
 * @param <V> type of view
 */
public abstract class Controller<V> {
    private boolean viewLoaded;
    private V view;
    private List<ControllerListener<V>> listenerList;

    public Controller() {
        this.listenerList = new ArrayList<ControllerListener<V>>();
    }

    public void addListener(ControllerListener<V> listener) {
        this.listenerList.add(listener);
    }

    public void removeListener(ControllerListener<V> listener) {
        this.listenerList.remove(listener);
    }

    /**
     * Loads view if not exists, and returns it.
     *
     * @return an loaded view
     */
    public V getView() {
        if (!isViewLoaded()) {
            this.setView(this.loadView());
        }
        return this.view;
    }

    /**
     * Set associated view
     *
     * @param view a View
     */
    protected void setView(V view) {
        V prevView = this.view;
        this.view = view;

        assert this.view != null;

        if (prevView == null) {
            viewLoaded = true;
            viewDidLoad();
        }
    }

    /**
     * Called when view requested.
     *
     * @return view that will be attached to the controller
     */
    protected abstract V loadView();

    /**
     * Called when view loading is finished.
     */
    protected abstract void viewDidLoad();

    /**
     * Defining the view-type specified view presentation method
     */
    protected abstract void presentView();

    public void present() {
        for (ControllerListener<V> listener : this.listenerList) {
            listener.controllerWillPresent(this);
        }

        this.presentView();

        for (ControllerListener<V> listener : this.listenerList) {
            listener.controllerDidPresent(this);
        }
    }

    public void dismiss() {
        for (ControllerListener<V> listener : this.listenerList) {
            listener.controllerWillDismiss(this);
        }

        this.dismissView();

        for (ControllerListener<V> listener : this.listenerList) {
            listener.controllerDidDismiss(this);
        }
    }

    /**
     * Defining the view-type specified view dismissing method
     */
    protected abstract void dismissView();

    /**
     * Indicates that is view loaded.
     *
     * @return the view loading statement.
     */
    public boolean isViewLoaded() {
        return this.viewLoaded;
    }


    /**
     * General purpose view listener
     *
     * @param <V> type of View
     */
    public static interface ControllerListener<V> {
        /**
         * Called before view presentation
         *
         * @param controller the controller
         */
        void controllerWillPresent(Controller<V> controller);

        /**
         * Called after view presentation
         *
         * @param controller the controller
         */
        void controllerDidPresent(Controller<V> controller);

        /**
         * Called before view dismissing
         *
         * @param controller the controller
         */
        void controllerWillDismiss(Controller<V> controller);

        /**
         * Called after view dismissing
         *
         * @param controller the controller
         */
        void controllerDidDismiss(Controller<V> controller);
    }
}
