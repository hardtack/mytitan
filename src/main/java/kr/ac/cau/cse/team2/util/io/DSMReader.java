package kr.ac.cau.cse.team2.util.io;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import com.google.common.collect.Table;
import kr.ac.cau.cse.team2.util.Pair;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads DSM file format.
 */
public class DSMReader {
    private BufferedReader reader;
    private int current = 0;

    private boolean eof(int c) {
        return c == -1;
    }


    private void skipWhiteSpaces() throws IOException {
        while (Character.isWhitespace(current) && !eof(current)) {
            current = reader.read();
        }
    }

    private int readInt() throws IOException, InvalidDSMFormatException {
        StringBuilder builder = new StringBuilder();
        while (Character.isDigit(current)) {
            builder.append((char) current);
            current = reader.read();
        }
        try {
            return Integer.parseInt(builder.toString());
        } catch (NumberFormatException e) {
            throw new InvalidDSMFormatException(e.getLocalizedMessage());
        }
    }

    private String readLine() throws IOException {
        if (eof(current)) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        while (current != '\n' && !eof(current)) {
            builder.append((char) current);
            current = reader.read();
        }
        int last = builder.length() - 1;
        if (builder.charAt(last) == '\r') { // CRLF support
            builder.deleteCharAt(last);
        }
        return builder.toString();
    }

    public Pair<Table<Integer, Integer, Boolean>, List<String>> readDSM(String filename) throws IOException, InvalidDSMFormatException {
        return readDSM(new File(filename));
    }

    public Pair<Table<Integer, Integer, Boolean>, List<String>> readDSM(File file) throws IOException, InvalidDSMFormatException {
        FileInputStream stream = new FileInputStream(file);
        try {
            return readDSM(stream);
        } finally {
            stream.close();
        }
    }

    public Pair<Table<Integer, Integer, Boolean>, List<String>> readDSMFromString(String string) throws InvalidDSMFormatException, IOException {
        ByteArrayInputStream stream = new ByteArrayInputStream(string.getBytes("utf-8"));
        try {
            return readDSM(stream);
        } finally {
            stream.close();
        }
    }

    public Pair<Table<Integer, Integer, Boolean>, List<String>> readDSM(InputStream stream) throws IOException, InvalidDSMFormatException {
        this.reader = new BufferedReader(new InputStreamReader(stream));
        current = reader.read();
        this.skipWhiteSpaces();

        int size = readInt();

        this.skipWhiteSpaces();

        Iterable<Integer> rowKeys = ContiguousSet.create(Range.closedOpen(0, size), DiscreteDomain.integers());
        Iterable<Integer> columnKeys = ContiguousSet.create(Range.closedOpen(0, size), DiscreteDomain.integers());

        ArrayTable<Integer, Integer, Boolean> matrix = ArrayTable.create(rowKeys, columnKeys);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int intValue = readInt();
                boolean value;
                if (intValue == 0) {
                    value = false;
                } else if (intValue == 1) {
                    value = true;
                } else {
                    throw new InvalidDSMFormatException("Invalid DSM Format");
                }
                matrix.put(i, j, value);
                this.skipWhiteSpaces();
            }
        }

        List<String> names = new ArrayList<String>(size);
        for (int i = 0; i < size; i++) {
            String line = readLine();
            if (line == null) {
                throw new InvalidDSMFormatException("Invalid DSM Format");
            }
            names.add(line);
            this.skipWhiteSpaces();
        }

        return new Pair<Table<Integer, Integer, Boolean>, List<String>>(matrix, names);
    }

    public static class InvalidDSMFormatException extends Exception {
        public InvalidDSMFormatException(String s) {
            super(s);
        }
    }
}
