package kr.ac.cau.cse.team2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

/**
 * Resource management
 */
public final class Resource {
    private static ClassLoader classLoader = Resource.class.getClassLoader();

    public static URL getResourceURL(String path){
        return classLoader.getResource(path);
    }

    public static InputStream getResourceStream(String path) throws FileNotFoundException {
        return new FileInputStream(getResourceFile(path));
    }

    public static File getResourceFile(String path) {
        return new File(getResourceURL(path).getFile());
    }

    public static boolean isResourceExists(String path) {
        return getResourceFile(path).exists();
    }
}
