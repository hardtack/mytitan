package kr.ac.cau.cse.team2.util.io;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Reads clustering file format.
 */
public class ClusteringReader {
    private Module readSingleModule(Node node, List<Module> origins) {
        final String name = node.getAttributes().getNamedItem("name").getNodeValue();
        Module module;
        try {
            module = Iterables.find(origins, new Predicate<Module>() {
                @Override
                public boolean apply(Module module) {
                    return module.getName().equals(name);
                }
            });
        } catch (NoSuchElementException e) {
            return null;
        }
        origins.remove(module);

        return module;
    }

    private GroupedModule readGroupedModule(Node node, List<Module> origins) throws InvalidClusteringFormatException {
        String name = node.getAttributes().getNamedItem("name").getNodeValue();
        GroupedModule group = new GroupedModule(name);

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            if (child.getNodeName().equals("#text")) {  // Ignore text node
                continue;
            }
            Module subModule;
            if ((subModule = readModule(child, origins)) != null) {
                group.addSubModule(subModule);
            }
        }

        return group;
    }

    private Module readModule(Node node, List<Module> origins) throws InvalidClusteringFormatException {
        if (node.getNodeName().equals("group")) {
            return readGroupedModule(node, origins);
        } else if (node.getNodeName().equals("item")) {
            return readSingleModule(node, origins);
        } else {
            throw new InvalidClusteringFormatException("Node name must be \"item\" or \"group\"");
        }
    }

    private List<Module> readClusteringWithExceptions(InputStream stream, List<Module> origins) throws ParserConfigurationException, IOException, SAXException, InvalidClusteringFormatException {
        List<Module> modules = new ArrayList<Module>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(stream);

        NodeList rootNodes = document.getElementsByTagName("cluster");
        if (rootNodes.getLength() != 1) {
            throw new InvalidClusteringFormatException("Has multiple root nodes");
        }
        Node root = rootNodes.item(0);

        NodeList nodes = root.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeName().equals("#text")) {  // Ignore text node
                continue;
            }
            modules.add(readModule(node, origins));
        }

        return modules;
    }

    public List<Module> readClustering(String filename, List<Module> origins) throws IOException, InvalidClusteringFormatException {
        return readClustering(new File(filename), origins);
    }

    public List<Module> readClustering(File file, List<Module> origins) throws IOException, InvalidClusteringFormatException {
        FileInputStream stream = new FileInputStream(file);
        try {
            return readClustering(stream, origins);
        } finally {
            stream.close();
        }
    }

    public List<Module> readClustering(InputStream stream, List<Module> origins) throws IOException, InvalidClusteringFormatException {
        try {
            return readClusteringWithExceptions(stream, origins);
        } catch (ParserConfigurationException e) {
            throw new InvalidClusteringFormatException();
        } catch (SAXException e) {
            throw new InvalidClusteringFormatException(e.getLocalizedMessage());
        }
    }

    public static class InvalidClusteringFormatException extends Exception {
        public InvalidClusteringFormatException() {
            super();
        }

        public InvalidClusteringFormatException(String s) {
            super(s);
        }
    }
}
