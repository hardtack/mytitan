package kr.ac.cau.cse.team2.util;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Table;
import com.google.common.primitives.Ints;

import java.util.Arrays;

/**
 * Provides matrix related utils.
 */
public final class MatrixUtil {
    private MatrixUtil() {
    }

    /**
     * Build a matrix from array.
     * Drop columns that whose values are not fully filled.
     *
     * @param rowArray an array
     * @param <T>      type of an array
     * @return a table as a matrix (ImmutableTable used for implementation)
     */
    public static <T> Table<Integer, Integer, T> arrayToMatrix(T[][] rowArray) {
        int rowCount = rowArray.length;
        int colCount = new Ordering<T[]>() {
            @Override
            public int compare(T[] left, T[] right) {
                return Ints.compare(left.length, right.length);
            }
        }.min(Arrays.asList(rowArray)).length;

        return arrayToMatrix(rowArray, rowCount, colCount, null);
    }

    /**
     * Build a matrix from array
     *
     * @param rowArray an array
     * @param <T>      type of an array
     * @param fill     fill remaining space with this value
     * @return a table as a matrix (ImmutableTable used for implementation)
     */
    public static <T> Table<Integer, Integer, T> arrayToMatrix(T[][] rowArray, T fill) {
        int rowCount = rowArray.length;
        int colCount = new Ordering<T[]>() {
            @Override
            public int compare(T[] left, T[] right) {
                return Ints.compare(left.length, right.length);
            }
        }.max(Arrays.asList(rowArray)).length;

        return arrayToMatrix(rowArray, rowCount, colCount, fill);
    }

    /**
     * Build a matrix from array
     *
     * @param rowArray an array
     * @param <T>      type of an array
     * @param rowSize  row size of matrix
     * @param colSize  col size of matrix
     * @param fill     fill remaining space with this value
     * @return a table as a matrix (ImmutableTable used for implementation)
     */
    public static <T> Table<Integer, Integer, T> arrayToMatrix(T[][] rowArray, int rowSize, int colSize, T fill) {
        Iterable<Integer> rowKeys = ContiguousSet.create(Range.closedOpen(0, rowSize), DiscreteDomain.integers());
        Iterable<Integer> columnKeys = ContiguousSet.create(Range.closedOpen(0, colSize), DiscreteDomain.integers());
        ArrayTable<Integer, Integer, T> matrix = ArrayTable.create(rowKeys, columnKeys);
        for (int rowIdx = 0; rowIdx < rowSize; rowIdx++) {
            for (int colIdx = 0; colIdx < colSize; colIdx++) {
                if (rowIdx < rowArray.length && colIdx < rowArray[rowIdx].length) {
                    matrix.put(rowIdx, colIdx, rowArray[rowIdx][colIdx]);
                } else {
                    matrix.put(rowIdx, colIdx, fill);
                }
            }
        }
        return matrix;
    }
}
