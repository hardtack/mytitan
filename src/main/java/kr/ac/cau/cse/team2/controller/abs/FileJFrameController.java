package kr.ac.cau.cse.team2.controller.abs;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Arrays;

/**
 * JFameController with file menu
 */
public class FileJFrameController extends JFrameController {
    public static final JMenu SEPARATOR_MENU_ITEM = new JMenu();
    private JMenu fileMenu;

    public JMenu getFileMenu() {
        return fileMenu;
    }

    public void setFileMenu(JMenu fileMenu) {
        this.fileMenu = fileMenu;
    }

    @Override
    protected JFrame loadView() {
        JFrame view = super.loadView();

        // File Menu
        this.fileMenu = new JMenu("File");
        this.getMenuBar().add(this.fileMenu);

        return view;
    }

    @Override
    protected void viewDidLoad() {
        super.viewDidLoad();
        for (JMenuItem item : this.fileMenuItems()) {
            if (item == SEPARATOR_MENU_ITEM) {
                this.fileMenu.addSeparator();
            } else {
                this.fileMenu.add(item);
            }
        }
    }

    /**
     * Override this method to add item to file menu.
     * You can re-order menu items by using this method.
     *
     * @return menu items for file menu
     */
    protected Iterable<JMenuItem> fileMenuItems() {
        JMenuItem colseItem = new JMenuItem(new AbstractAction("Close") {
            public void actionPerformed(ActionEvent e) {
                dismiss();
            }
        });
        colseItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_W,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()
        ));
        return Arrays.asList(colseItem);
    }
}
