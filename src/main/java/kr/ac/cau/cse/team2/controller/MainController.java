package kr.ac.cau.cse.team2.controller;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.google.common.io.PatternFilenameFilter;
import kr.ac.cau.cse.team2.controller.abs.FileJFrameController;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import kr.ac.cau.cse.team2.model.operation.DSM;
import kr.ac.cau.cse.team2.model.operation.ModuleTraversable;
import kr.ac.cau.cse.team2.util.Pair;
import kr.ac.cau.cse.team2.util.Resource;
import kr.ac.cau.cse.team2.util.function.HighOrder;
import kr.ac.cau.cse.team2.util.io.ClusteringReader;
import kr.ac.cau.cse.team2.util.io.ClusteringWriter;
import kr.ac.cau.cse.team2.util.io.DSMReader;
import kr.ac.cau.cse.team2.util.io.DSMWriter;
import kr.ac.cau.cse.team2.view.SelectionColorTree;
import kr.ac.cau.cse.team2.view.event.TreeMutationListener;

import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Main controller
 */
public class MainController extends FileJFrameController implements TreeCellRenderer, ListCellRenderer<Module> {
    private final TreeExpansionListener treeExpansionListener = new TreeExpansionListener() {
        @Override
        public void treeExpanded(TreeExpansionEvent event) {
            Object item = ((DefaultMutableTreeNode) event.getPath().getLastPathComponent()).getUserObject();
            if (item instanceof GroupedModule) {
                GroupedModule group = (GroupedModule) item;
                group.setOpen(true);
            }
            updateRight();
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event) {
            Object item = ((DefaultMutableTreeNode) event.getPath().getLastPathComponent()).getUserObject();
            if (item instanceof GroupedModule) {
                GroupedModule group = (GroupedModule) item;
                group.setOpen(false);
            }
            updateRight();
        }
    };
    private final TreeMutationListener treeMutationListener = new TreeMutationListener() {
        @Override
        public void willUpdate(JTree tree) {

        }

        @Override
        public void didUpdate(JTree tree) {
            updateUI();
        }

        @Override
        public void willRemoveChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child) {

        }

        @Override
        public void didRemoveChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child) {
            GroupedModule group = (GroupedModule) parent.getUserObject();
            Module module = (Module) child.getUserObject();
            group.removeSubModule(module);
        }

        @Override
        public void willAddChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child, int index) {

        }

        @Override
        public void didAddChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child, int index) {
            GroupedModule group = (GroupedModule) parent.getUserObject();
            Module module = (Module) child.getUserObject();

            group.addSubModule(index, module);

        }
    };
    private final TreeWillExpandListener treeWillExpansionListener = new TreeWillExpandListener() {
        @Override
        public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {

        }

        @Override
        public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
        }
    };
    private JMenuItem newDSMItem;
    private JMenuItem newClusteringItem;
    private JMenuItem openDSMItem;
    private JMenuItem openClusteringItem;

    private JMenu viewMenu;
    private JCheckBoxMenuItem showRowLabelsItem;

    private JMenu helpMenu;
    private JMenuItem aboutItem;

    private JSplitPane mainLayoutSplitPane;
    private JPanel leftPanel;
    private JPanel leftTopPanel;
    private JToolBar leftToolBar;
    private JButton expandAllButton;
    private JButton collapseAllButton;
    private JScrollPane leftTreeScollPane;
    private SelectionColorTree leftTree;
    private JPanel rightPanel;
    private JSplitPane rightSplitPane;
    private JPanel rightModuleListPanel;
    private JScrollPane rightModuleListScrollPane;
    private JList<Module> rightModuleList;
    private JTable dsmTable;
    private JScrollPane dsmTableScrollPane;
    private Table<Integer, Integer, Boolean> dsm = new ImmutableTable.Builder<Integer, Integer, Boolean>().build();
    private List<Module> moduleList = new ArrayList<Module>(0);
    private boolean showRowLabels = true;

    private JMenuItem writeDSMItem;
    private JMenuItem writeDSMAsItem;
    private File dsmFile;
    private final Action openDSMAction = new AbstractAction("Open DSM") {
        @Override
        public void actionPerformed(ActionEvent e) {
            // AWT's file dialog is much better than Swing's one.
            FileDialog fileDialog = new java.awt.FileDialog(getView(), "Choose DSM file");

            fileDialog.setFilenameFilter(new PatternFilenameFilter("^.*.dsm"));
            fileDialog.setVisible(true);
            File files[] = fileDialog.getFiles();
            if (files.length == 0) {
                return;
            }
            File file = files[0];
            Pair<Table<Integer, Integer, Boolean>, List<String>> pair;
            try {
                pair = new DSMReader().readDSM(file);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(getView(), "Error occurred while reading DSM file.");
                return;
            } catch (DSMReader.InvalidDSMFormatException ex) {
                JOptionPane.showMessageDialog(getView(), "The selected DSM file is invalid.");
                return;
            }
            dsmFile = file;
            List<Module> modules = DSM.createModules(pair.getLeft(), pair.getRight());
            rootModule.setSubModules(modules);
            updateUI();
        }
    };
    private JMenuItem writeClusteringItem;
    private JMenuItem writeClusteringAsItem;
    private File clusteringFile;
    private int gridSize = 25;
    private final TableCellRenderer dsmColumnRenderer = new DefaultTableCellRenderer() {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JPanel cell = new JPanel();
            cell.setOpaque(false);
            cell.setLayout(new BorderLayout());

            Integer index = (Integer) value;
            JLabel label = new JLabel("" + (index + 1), JLabel.CENTER);
            cell.add(label, BorderLayout.CENTER);

            cell.setPreferredSize(new Dimension(cell.getPreferredSize().width, gridSize));
            return cell;
        }
    };
    private GroupedModule rootModule;
    private final MouseListener treeMouseListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e)) {
                TreePath clickedPath = leftTree.getClosestPathForLocation(e.getX(), e.getY());

                List<TreePath> selectionPaths = Lists.newArrayList(
                        Objects.firstNonNull(leftTree.getSelectionPaths(), new TreePath[0])
                );

                if (!selectionPaths.contains(clickedPath)) {
                    // New selection
                    selectionPaths = Lists.newArrayList(clickedPath);
                    leftTree.setSelectionPath(clickedPath);
                }
                final List<Module> selectedModules = HighOrder.map(selectionPaths, new Function<TreePath, Module>() {
                    @Override
                    public Module apply(TreePath input) {
                        return (Module) ((DefaultMutableTreeNode) input.getLastPathComponent()).getUserObject();
                    }
                });

                final GroupedModule parent = getParent(selectedModules.get(0));

                // Menu
                JPopupMenu menu = new JPopupMenu("Menu");


                if (selectedModules.size() == 1) {
                    final Module module = selectedModules.get(0);

                    // Add
                    if (module instanceof GroupedModule) {
                        final GroupedModule group = (GroupedModule) module;

                        // Add
                        JMenuItem addItem = new JMenuItem("Add");
                        addItem.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String newName = JOptionPane.showInputDialog("New name");
                                if (newName == null || (newName = newName.trim()).isEmpty()) {
                                    return;
                                }
                                Module module = new Module(newName);
                                group.addSubModule(module);
                                updateUI();
                            }
                        });
                        menu.add(addItem);

                        // Ungroup
                        if (group != rootModule) {
                            JMenuItem ungroupItem = new JMenuItem("Ungroup");
                            ungroupItem.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    String title = "Ungroup";
                                    String message = "Do you really want to ungroup?";

                                    int rv = JOptionPane.showConfirmDialog(getView(), message, title, JOptionPane.OK_CANCEL_OPTION);
                                    if (rv == JOptionPane.OK_OPTION) {
                                        List<Module> subModules = group.getSubModules();
                                        final int index = parent.getSubModules().indexOf(group);
                                        parent.removeSubModule(group);
                                        HighOrder.foreach(subModules, new HighOrder.ForEach<Module>() {
                                            public void apply(int i, Module subModule) {
                                                parent.addSubModule(index + i, subModule);
                                            }
                                        });
                                        updateUI();
                                    }
                                }
                            });
                            menu.add(ungroupItem);
                        }
                    }

                    // Rename
                    if (module != rootModule) {
                        JMenuItem renameItem = new JMenuItem("Rename");
                        renameItem.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String newName = JOptionPane.showInputDialog("New name", module.getName());
                                if (newName == null || (newName = newName.trim()).isEmpty()) {
                                    return;
                                }
                                module.setName(newName);
                                updateUI();
                            }
                        });
                        menu.add(renameItem);
                    }
                }

                // Do they have same parent?
                if (selectedModules.size() > 1 && Predicates.and(Lists.transform(
                        selectedModules.subList(1, selectedModules.size() - 1),
                        new Function<Module, Predicate<GroupedModule>>() {
                            @Override
                            public Predicate<GroupedModule> apply(Module input) {
                                return Predicates.equalTo(getParent(input));
                            }
                        })).apply(parent)) {
                    // Group
                    JMenuItem groupItem = new JMenuItem("Group");
                    groupItem.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            String groupName = JOptionPane.showInputDialog("New group name");
                            if (groupName == null || (groupName = groupName.trim()).isEmpty()) {
                                return;
                            }
                            GroupedModule group = new GroupedModule(groupName, selectedModules);

                            int index = parent.getSubModules().indexOf(selectedModules.get(0));
                            for (Module module : selectedModules) {
                                parent.removeSubModule(module);
                            }
                            parent.addSubModule(index, group);
                            updateUI();
                        }
                    });
                    menu.add(groupItem);
                }

                if (!selectedModules.contains(rootModule)) {
                    JMenuItem deleteItem = new JMenuItem("Delete");
                    deleteItem.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            String title;
                            String message;
                            if (selectedModules.size() > 1) {
                                title = "Delete items";
                                message = "Do you really want to delete these items?";
                            } else {
                                title = "Delete item";
                                message = "Do you really want to delete this item?";
                            }
                            int rv = JOptionPane.showConfirmDialog(getView(), message, title, JOptionPane.OK_CANCEL_OPTION);
                            if (rv == JOptionPane.OK_OPTION) {
                                for (Module module : selectedModules) {
                                    deleteModule(module);
                                }
                                updateUI();
                            }
                        }
                    });
                    menu.add(deleteItem);
                }

                menu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    };
    private final Action openClusteringAction = new AbstractAction("Open Clustering") {
        @Override
        public void actionPerformed(ActionEvent e) {
            // AWT's file dialog is much better than Swing's one.
            FileDialog fileDialog = new java.awt.FileDialog(getView(), "Choose clustering file");

            fileDialog.setFilenameFilter(new PatternFilenameFilter("^.*.clsx"));
            fileDialog.setVisible(true);
            File files[] = fileDialog.getFiles();
            if (files.length == 0) {
                return;
            }
            File file = files[0];
            List<Module> origins = DSM.leafModules(rootModule);
            List<Module> result;
            try {
                result = new ClusteringReader().readClustering(file, origins);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(getView(), "Error occurred while reading clustering file.");
                return;
            } catch (ClusteringReader.InvalidClusteringFormatException ex) {
                JOptionPane.showMessageDialog(getView(), "The selected clustering file is invalid.");
                return;
            }
            try {
                GroupedModule newRoot = (GroupedModule) result.get(0);
                rootModule.setSubModules(newRoot.getSubModules());
            } catch (IndexOutOfBoundsException ex) {
                JOptionPane.showMessageDialog(getView(), "The selected clustering file is invalid.");
                return;
            } catch (ClassCastException ex) {
                JOptionPane.showMessageDialog(getView(), "The selected clustering file is invalid.");
                return;
            }
            updateUI();
        }
    };

    private JButton imageButton(JButton button, String imagePath, int width, int height, String fallBack) {
        if (Resource.isResourceExists(imagePath)) {
            button.setIcon(new ImageIcon(Resource.getResourceURL(imagePath)));
        } else {
            button.setText(fallBack);
        }
        button.setMinimumSize(new Dimension(width, height));
        return button;
    }

    private File saveDSM(File file) {
        File prevFile = null;
        if (file == null) {
            while (true) {
                FileDialog fileDialog = new java.awt.FileDialog(getView(), "Save DSM file", FileDialog.SAVE);

                fileDialog.setFilenameFilter(new PatternFilenameFilter("^.*.dsm"));
                fileDialog.setFile(file != null ? file.getName() : ".dsm");
                fileDialog.setVisible(true);
                File files[] = fileDialog.getFiles();
                if (files.length == 0) {
                    return null;
                }
                file = files[0];
                if (file.getName().endsWith(".dsm")) {
                    break;
                }
                String message = String.format("\"%s\" does not end with \".dsm\"", file.getName());
                JOptionPane.showMessageDialog(
                        getView(),
                        message,
                        "File name must be end with \".dsm\"",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        } else {
            prevFile = file;
        }
        try {
            List<Module> saveModuleList = DSM.leafModules(rootModule);
            Table<Integer, Integer, Boolean> dsm = DSM.createDSM(saveModuleList);
            new DSMWriter().writeDSM(dsm, HighOrder.map(saveModuleList, new Function<Module, String>() {
                @Override
                public String apply(Module module) {
                    return module.getName();
                }
            }), file);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(getView(), e.getLocalizedMessage(),
                    "Error occurred while writing DSM file.",
                    JOptionPane.ERROR_MESSAGE);
            return prevFile;
        }
        return file;
    }

    private File saveDSM() {
        return saveDSM(null);
    }

    private File saveClustering(File file) {
        File prevFile = null;
        if (file == null) {
            while (true) {
                FileDialog fileDialog = new java.awt.FileDialog(getView(), "Save clustering file", FileDialog.SAVE);

                fileDialog.setFilenameFilter(new PatternFilenameFilter("^.*.clsx"));
                fileDialog.setFile(file != null ? file.getName() : ".clsx");
                fileDialog.setVisible(true);
                File files[] = fileDialog.getFiles();
                if (files.length == 0) {
                    return null;
                }
                file = files[0];
                if (file.getName().endsWith(".clsx")) {
                    break;
                }
                String message = String.format("\"%s\" does not end with \".clsx\"", file.getName());
                JOptionPane.showMessageDialog(
                        getView(),
                        message,
                        "File name must be end with \".clsx\"",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        } else {
            prevFile = file;
        }
        try {
            new ClusteringWriter().writeClustering(rootModule, file);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(getView(), e.getLocalizedMessage(),
                    "Error occurred while writing clustering file.",
                    JOptionPane.ERROR_MESSAGE);
            return prevFile;
        }
        return file;
    }


    private File saveClustering() {
        return saveClustering(null);
    }

    @Override
    protected JFrame loadView() {
        JFrame view = super.loadView();
        int width = 900;
        int height = 450;
        view.setSize(width, height);
        view.setMinimumSize(new Dimension(600, height));

        // Data
        this.rootModule = new GroupedModule("$root", true);

        // New DSM
        this.newDSMItem = new JMenuItem(new AbstractAction("New DSM") {
            @Override
            public void actionPerformed(ActionEvent e) {
                int n;
                String message = "Number of entries";
                String enter = "1";
                while (true) {
                    enter = JOptionPane.showInputDialog(message, enter);
                    if (enter == null) {
                        return;
                    }
                    try {
                        n = Integer.parseInt(enter);
                    } catch (NumberFormatException exception) {
                        message = "Number of entries\nPlease enter a number";
                        continue;
                    }
                    if (n >= 1000) {
                        int rv = JOptionPane.showConfirmDialog(getView(), "The number of entries is very large (%d)\n" +
                                "It can take very long time to process.\n" +
                                "Do you want to continue with this number?", "Warning", JOptionPane.YES_NO_OPTION);
                        if (rv == JOptionPane.NO_OPTION) {
                            continue;
                        }
                    }
                    break;
                }
                rootModule.setSubModules(new ArrayList<Module>());
                for (int i = 0; i < n; i++) {
                    Module module = new Module("entry_" + (i + 1));
                    rootModule.addSubModule(module);
                }
                updateUI();
            }
        });
        this.newClusteringItem = new JMenuItem(new AbstractAction("New Clustering") {
            @Override
            public void actionPerformed(ActionEvent e) {
                rootModule.setSubModules(DSM.leafModules(rootModule));
                updateUI();
            }
        });

        // Read DSM
        this.openDSMItem = new JMenuItem(this.openDSMAction);
        this.openDSMItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()
        ));

        // Open clustering
        this.openClusteringItem = new JMenuItem(this.openClusteringAction);

        this.openClusteringItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() |
                        InputEvent.SHIFT_MASK

        ));

        // Write DSM
        this.writeDSMItem = new JMenuItem(new AbstractAction("Save DSM") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dsmFile = saveDSM(dsmFile);
            }
        });

        this.writeDSMItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_D,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()
        ));

        this.writeDSMAsItem = new JMenuItem(new AbstractAction("Save DSM As...") {
            @Override
            public void actionPerformed(ActionEvent e) {
                File out = saveDSM();
                if (out != null) {
                    dsmFile = out;
                }
            }
        });

        this.writeDSMAsItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_D,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() |
                        InputEvent.SHIFT_MASK
        ));

        // Clustering...
        this.writeClusteringItem = new JMenuItem(new AbstractAction("Save Clustering") {
            @Override
            public void actionPerformed(ActionEvent e) {
                clusteringFile = saveClustering(clusteringFile);
            }
        });

        this.writeClusteringItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()
        ));

        this.writeClusteringAsItem = new JMenuItem(new AbstractAction("Save Clustering As...") {
            @Override
            public void actionPerformed(ActionEvent e) {
                clusteringFile = saveClustering(clusteringFile);
            }
        });

        this.writeClusteringAsItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S,
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() |
                        InputEvent.SHIFT_MASK
        ));

        // View Menu
        this.viewMenu = new JMenu("View");
        this.showRowLabelsItem = new JCheckBoxMenuItem(new AbstractAction("Show Row Labels") {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainController.this.showRowLabels = showRowLabelsItem.getState();
                updateRightModuleList();
            }
        });
        this.showRowLabelsItem.setState(true);
        this.viewMenu.add(showRowLabelsItem);
        this.getMenuBar().add(this.viewMenu);

        // Help Menu
        this.helpMenu = new JMenu("Help");
        this.aboutItem = new JMenuItem(new AbstractAction("About...") {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(getView(),
                        "Titan\n" +
                                "Team: 2\n" +
                                "20125012 최건우\n" +
                                "20120355 강동수\n" +
                                "20101547 엄기용");
            }
        });
        this.helpMenu.add(this.aboutItem);
        this.getMenuBar().add(this.helpMenu);

        // Left
        this.leftPanel = new JPanel();
        this.leftPanel.setMinimumSize(new Dimension(200, height));
        this.leftPanel.setLayout(new BorderLayout());

        // Left top
        this.leftTopPanel = new JPanel();
        this.leftTopPanel.setLayout(new BoxLayout(this.leftTopPanel, BoxLayout.LINE_AXIS));
        this.leftPanel.add(this.leftTopPanel, BorderLayout.NORTH);

        // Tool bar
        this.leftToolBar = new JToolBar();
        this.leftToolBar.setPreferredSize(new Dimension(200, gridSize));
        this.leftTopPanel.add(leftToolBar);
        this.leftToolBar.setFloatable(false);

        this.expandAllButton = imageButton(new JButton(), "images/expand.png", gridSize, gridSize, "Expand All");
        this.collapseAllButton = imageButton(new JButton(), "images/collapse.png", gridSize, gridSize, "Collapse All");

        this.expandAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                expandAll();
            }
        });
        this.collapseAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                collapseAll();
            }
        });

        this.leftToolBar.add(this.expandAllButton);
        this.leftToolBar.add(this.collapseAllButton);

        // Tree
        this.leftTree = new SelectionColorTree(this.createNode(rootModule));

        // Scroll
        this.leftTreeScollPane = new JScrollPane(leftTree);
        this.leftPanel.add(this.leftTreeScollPane, BorderLayout.CENTER);

        // Right
        this.rightPanel = new JPanel();
        this.rightPanel.setSize(700, height);
        this.rightPanel.setLayout(new BorderLayout());

        // Right list
        this.rightModuleList = new JList<Module>(new AbstractListModel<Module>() {
            @Override
            public int getSize() {
                return moduleList.size();
            }

            @Override
            public Module getElementAt(int index) {
                return moduleList.get(index);
            }
        });
        this.rightModuleList.setSize(200, height);

        // Right module scroll
        this.rightModuleListScrollPane = new JScrollPane(this.rightModuleList);

        // Right module list container
        this.rightModuleListPanel = new JPanel();
        this.rightModuleListPanel.setLayout(new BorderLayout());
        this.rightModuleListPanel.setOpaque(false);
        this.rightModuleListPanel.add(this.rightModuleListScrollPane, BorderLayout.CENTER);

        // Top Padding
        this.rightModuleListPanel.setBorder(new EmptyBorder(gridSize, 0, 0, 0));

        this.rightModuleListPanel.setMinimumSize(new Dimension(200, height));
        this.rightModuleListPanel.setSize(200, height);

        // DSM
        this.dsmTable = new JTable(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return dsm.rowKeySet().size();
            }

            @Override
            public int getColumnCount() {
                return dsm.columnKeySet().size();
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return dsm.get(rowIndex, columnIndex);
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                if (rowIndex == columnIndex) {
                    return false;
                }
                Module from = moduleList.get(rowIndex);
                Module to = moduleList.get(columnIndex);
                if (from instanceof GroupedModule || to instanceof GroupedModule) {
                    return false;
                }
                return true;
            }

            @Override
            public void setValueAt(Object value, int rowIndex, int columnIndex) {
                Boolean dependency = (Boolean) value;
                Module from = moduleList.get(rowIndex);
                Module to = moduleList.get(columnIndex);
                if (dependency) {
                    from.addDependency(to);
                } else {
                    from.removeDependency(to);
                }
                updateRight();
                fireTableCellUpdated(rowIndex, columnIndex);
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return Boolean.class;
            }
        });
        this.dsmTable.setSelectionBackground(this.dsmTable.getBackground());
        this.dsmTable.setSelectionForeground(this.dsmTable.getForeground());
        this.dsmTable.setSize(500, height);


        // Scroll
        this.dsmTableScrollPane = new JScrollPane(this.dsmTable);
        this.dsmTableScrollPane.setMinimumSize(new Dimension(200, height));
        this.dsmTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);


        // Split
        this.rightSplitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT,
                this.rightModuleListPanel,
                this.dsmTableScrollPane
        );
        this.rightSplitPane.setResizeWeight((float)
                this.rightModuleListScrollPane.getWidth() /
                (this.rightModuleListScrollPane.getWidth() + this.dsmTableScrollPane.getWidth()));
        this.rightSplitPane.setContinuousLayout(true);
        this.rightPanel.add(this.rightSplitPane);

        // Layout
        this.mainLayoutSplitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT,
                this.leftPanel,
                this.rightPanel
        );
        this.mainLayoutSplitPane.setResizeWeight((float)
                this.leftPanel.getWidth() /
                (this.leftPanel.getWidth() + this.rightPanel.getWidth()));
        this.mainLayoutSplitPane.setContinuousLayout(true);
        view.add(this.mainLayoutSplitPane);

        return view;
    }

    @Override
    protected void viewDidLoad() {
        super.viewDidLoad();
        this.getView().addWindowListener(this);

        // Init module list
        this.rightModuleList.setCellRenderer(this);

        // Init Tree
        this.leftTree.setSelectionColor(this.rightModuleList.getSelectionBackground());
        this.leftTree.setCellRenderer(this);
        this.leftTree.addTreeExpansionListener(this.treeExpansionListener);
        this.leftTree.addTreeWillExpandListener(this.treeWillExpansionListener);
        this.leftTree.addMouseListener(this.treeMouseListener);
        this.leftTree.addTreeMutationListener(this.treeMutationListener);

        // Scroll handler
        this.rightModuleListScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                int y = rightModuleListScrollPane.getVerticalScrollBar().getValue();
                if (y != dsmTableScrollPane.getVerticalScrollBar().getValue()) {
                    dsmTableScrollPane.getVerticalScrollBar().setValue(y);
                }
            }
        });
        this.dsmTableScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                int y = dsmTableScrollPane.getVerticalScrollBar().getValue();
                if (y != rightModuleListScrollPane.getVerticalScrollBar().getValue()) {
                    rightModuleListScrollPane.getVerticalScrollBar().setValue(y);
                }
            }
        });

        updateUI();
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        Module module = (Module) node.getUserObject();
        // Cell base
        JPanel cell = new JPanel();
        if (selected) {
            cell.setBackground(leftTree.getSelectionColor());
        } else {
            cell.setBackground(tree.getBackground());
        }
        if (leftTree.getDropTargetObject() == value) {
            if (leftTree.isDropTargetLeaf()) {
                cell.setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createMatteBorder(0, 1, 0, 0, Color.BLACK),
                        BorderFactory.createEmptyBorder(1, 0, 1, 1)
                ));
            } else {
                cell.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
            }
        } else {
            cell.setBorder(new EmptyBorder(1, 1, 1, 1));
        }
        cell.setLayout(new BorderLayout());

        // Cell content
        JPanel cellContent = new JPanel();
        cellContent.setBackground(cell.getBackground());
        cellContent.setBorder(new EmptyBorder(1, 4, 1, 4));
        cellContent.setLayout(new BorderLayout());
        cell.add(cellContent, BorderLayout.CENTER);

        // Label
        JLabel label = new JLabel(module.getName());
        cellContent.add(label, BorderLayout.CENTER);
        if (selected) {
            label.setForeground(rightModuleList.getSelectionForeground());
        } else {
            label.setForeground(tree.getForeground());
        }

        return cell;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Module> list, Module module, int index, boolean selected, boolean cellHasFocus) {
        // Cell base
        JPanel cell = new JPanel();
        if (index % 2 == 0) {
            cell.setBackground(list.getBackground());
        } else {
            Color color = list.getSelectionBackground();
            cell.setBackground(new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) (color.getAlpha() * 0.2)));
        }
        cell.setLayout(new BorderLayout());

        // Cell content
        JPanel cellContent = new JPanel();
        cellContent.setBackground(cell.getBackground());
        cellContent.setBorder(new EmptyBorder(1, 4, 1, 4));
        cellContent.setLayout(new BorderLayout());
        cell.add(cellContent, BorderLayout.CENTER);

        // Label
        JLabel label = new JLabel();
        if (this.showRowLabels) {
            label.setText(String.format("%d. %s", index + 1, module.getName()));
        } else {
            label.setText((index + 1) + "");
        }

        cellContent.add(label, BorderLayout.CENTER);
        label.setForeground(list.getForeground());

        // Size
        cell.setPreferredSize(new Dimension(list.getWidth(), gridSize));
        return cell;
    }

    private GroupedModule getParent(Module module, GroupedModule group) {
        if (group.getSubModules().contains(module)) {
            return group;
        }
        for (Module subModule : group.getSubModules()) {
            if (module == subModule) {
                return group;
            }
            if (subModule instanceof GroupedModule) {
                GroupedModule parent = getParent(module, (GroupedModule) subModule);
                if (parent != null) {
                    return parent;
                }
            }
        }
        return null;
    }

    private GroupedModule getParent(Module module) {
        return getParent(module, rootModule);
    }

    private DefaultMutableTreeNode createNode(Module module) {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(module);
        node.setAllowsChildren(false);
        if (module instanceof GroupedModule) {
            GroupedModule group = (GroupedModule) module;
            node.setAllowsChildren(true);
            for (Module subModule : group.getSubModules()) {
                node.add(createNode(subModule));
            }
        }
        return node;
    }

    private void expandAll() {
        this.expandAll(rootModule);
        updateUI();
    }

    private void expandAll(GroupedModule group) {
        group.setOpen(true);
        for (Module subModule : group.getSubModules()) {
            if (subModule instanceof GroupedModule) {
                expandAll((GroupedModule) subModule);
            }
        }
    }

    private void collapseAll() {
        this.collapseAll(rootModule);
        updateUI();
    }

    private void collapseAll(GroupedModule group) {
        group.setOpen(false);
        for (Module subModule : group.getSubModules()) {
            if (subModule instanceof GroupedModule) {
                collapseAll((GroupedModule) subModule);
            }
        }
    }

    private void updateExpansion(DefaultMutableTreeNode node) {
        if (node.getAllowsChildren()) {
            GroupedModule group = (GroupedModule) node.getUserObject();
            TreePath path = new TreePath(node.getPath());
            if (group.isOpen()) {
                leftTree.expandPath(path);
                if (node.getChildCount() >= 0) {
                    Enumeration enumeration = node.children();
                    while (enumeration.hasMoreElements()) {
                        DefaultMutableTreeNode subNode = (DefaultMutableTreeNode) enumeration.nextElement();
                        updateExpansion(subNode);
                    }
                }
            } else {
                if (node.getChildCount() >= 0) {
                    Enumeration enumeration = node.children();
                    while (enumeration.hasMoreElements()) {
                        DefaultMutableTreeNode subNode = (DefaultMutableTreeNode) enumeration.nextElement();
                        updateExpansion(subNode);
                    }
                }
                leftTree.collapsePath(path);
            }
        }
    }

    private void updateTree() {
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) this.leftTree.getModel().getRoot();
        rootNode.removeAllChildren();
        for (Module subModule : this.rootModule.getSubModules()) {
            rootNode.add(createNode(subModule));
        }
        updateExpansion(rootNode);
        this.leftTree.updateUI();
    }


    private void updateMatrix() {
        this.moduleList = Lists.newArrayList(ModuleTraversable.traverse(this.rootModule));
        Table<Integer, Integer, Boolean> newDSM = DSM.createDSM(rootModule);
        if (this.dsm != null && this.dsm.equals(newDSM)) {
            return;
        }
        this.dsm = newDSM;

        AbstractTableModel tableModel = (AbstractTableModel) this.dsmTable.getModel();
        tableModel.fireTableStructureChanged();
        TableColumnModel tableColumnModel = this.dsmTable.getColumnModel();
        for (int i = 0; i < tableColumnModel.getColumnCount(); i++) {
            TableColumn column = tableColumnModel.getColumn(i);
            column.setHeaderValue(i);
            column.setHeaderRenderer(this.dsmColumnRenderer);
            column.setPreferredWidth(gridSize);
        }
        this.dsmTable.setRowHeight(gridSize);
        this.dsmTable.repaint();
    }

    private void updateRightModuleList() {
        this.rightModuleList.updateUI();
    }

    private void updateRight() {
        updateMatrix();
        updateRightModuleList();
    }

    private void updateUI() {
        updateRight();
        updateTree();
    }

    private void cleanDependency(Module module, Module from) {
        if (from instanceof GroupedModule) {
            GroupedModule group = (GroupedModule) from;
            for (Module subModule : group.getSubModules()) {
                this.cleanDependency(module, subModule);
            }
        } else {
            from.removeDependency(module);
        }
    }

    private void deleteModule(Module module) {
        GroupedModule parent = getParent(module);
        deleteModule(module, parent);
    }

    private void deleteModule(Module module, GroupedModule parent) {
        if (parent == null) {
            return;
        }
        parent.removeSubModule(module);
        this.cleanDependency(module, this.rootModule);
    }

    @Override
    protected Iterable<JMenuItem> fileMenuItems() {
        return Iterables.concat(Arrays.asList(
                        this.newDSMItem,
                        SEPARATOR_MENU_ITEM,
                        this.openDSMItem,
                        this.writeDSMItem,
                        this.writeDSMAsItem,
                        SEPARATOR_MENU_ITEM,
                        this.newClusteringItem,
                        SEPARATOR_MENU_ITEM,
                        this.openClusteringItem,
                        this.writeClusteringItem,
                        this.writeClusteringAsItem,
                        SEPARATOR_MENU_ITEM),
                super.fileMenuItems());
    }
}
