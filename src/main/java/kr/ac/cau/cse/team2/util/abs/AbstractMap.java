package kr.ac.cau.cse.team2.util.abs;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Abstracted map
 */
public abstract class AbstractMap<K, V> implements Map<K, V> {
    public int size() {
        return keySet().size();
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return values().contains(value);
    }

    public abstract V get(Object key);

    public abstract V put(K key, V value);

    public abstract V remove(Object key);

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        for (K key : this.keySet()) {
            this.remove(key);
        }
    }

    public abstract Set<K> keySet();

    @Override
    public Collection<V> values() {
        return Collections2.transform(this.entrySet(), new Function<Entry<K, V>, V>() {
            @Override
            public V apply(Entry<K, V> input) {
                return input.getValue();
            }
        });
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return FluentIterable.from(this.keySet()).transform(new Function<K, Entry<K, V>>() {
            @Override
            public Entry<K, V> apply(final K input) {
                return new Entry<K, V>() {
                    @Override
                    public K getKey() {
                        return input;
                    }

                    @Override
                    public V getValue() {
                        return get(input);
                    }

                    @Override
                    public V setValue(V value) {
                        return put(input, value);
                    }
                };
            }
        }).toSet();
    }
}
