package kr.ac.cau.cse.team2.view;

import com.google.common.base.Objects;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * DnD Tree that using customized selection color in entire row
 */
public class SelectionColorTree extends DnDJTree {
    private Color selectionColor = Color.BLUE;



    public SelectionColorTree() {
        super();
        this.setOpaque(false);
    }

    public SelectionColorTree(TreeNode root) {
        super(root);
        this.setOpaque(false);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());
        if (getSelectionCount() > 0) {
            for (int i : Objects.firstNonNull(getSelectionRows(), new int[0])) {
                Rectangle r = getRowBounds(i);
                g.setColor(selectionColor);
                g.fillRect(0, r.y, getWidth(), r.height);
            }
        }
        super.paintComponent(g);

        if (Objects.firstNonNull(getSelectionPaths(), new TreePath[0]).length > 1 && getLeadSelectionPath() != null) {
            Rectangle r = getRowBounds(getRowForPath(getLeadSelectionPath()));
            if (r != null) {
                g.setColor(selectionColor.darker());
                g.drawRect(0, r.y, getWidth() - 1, r.height - 1);
            }
        }
    }

    public Color getSelectionColor() {
        return selectionColor;
    }

    public void setSelectionColor(Color selectionColor) {
        this.selectionColor = selectionColor;
    }
}
