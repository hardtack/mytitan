package kr.ac.cau.cse.team2.util;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Range;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Sparse map
 * Since there's no standard interface for fixed-size array, we use map.
 */
public class SparseMap<V> extends HashMap<Integer, V> {
    private int size;
    private V zeroValue;

    public SparseMap(int size, V zeroValue) {
        this.size = size;
        this.zeroValue = zeroValue;

    }

    public int size() {
        return size;
    }

    public V getZeroValue() {
        return zeroValue;
    }

    @Override
    public boolean containsKey(Object key) {
        if (!(key instanceof Integer)) {
            return false;
        }
        Integer index = (Integer) key;
        return index < size;
    }

    @Override
    public Set<Entry<Integer, V>> entrySet() {
        return FluentIterable.from(keySet()).transform(new Function<Integer, Entry<Integer, V>>() {
            @Override
            public Entry<Integer, V> apply(final Integer input) {
                return new Entry<Integer, V>() {
                    @Override
                    public Integer getKey() {
                        return input;
                    }

                    @Override
                    public V getValue() {
                        return get(input);
                    }

                    @Override
                    public V setValue(V value) {
                        return put(input, value);
                    }
                };
            }
        }).toSet();
    }

    @Override
    public Set<Integer> keySet() {
        return ContiguousSet.create(Range.closedOpen(0, size), DiscreteDomain.integers());
    }

    @Override
    public Collection<V> values() {
        return Collections2.transform(ContiguousSet.create(Range.closedOpen(0, size), DiscreteDomain.integers()),
                new Function<Integer, V>() {
                    @Override
                    public V apply(Integer input) {
                        return get(input);
                    }
                });
    }

    @Override
    public V get(Object key) {
        if (!(key instanceof Integer)) {
            return null;
        }
        Integer index = (Integer) key;
        if (index >= size) {
            return null;
        }
        V value = super.get(index);
        if (value == null) {
            value = getZeroValue();
        }
        return value;
    }

    @Override
    public V put(Integer key, V value) {
        if (key >= size) {
            throw new IndexOutOfBoundsException();
        }
        V prev = super.put(key, value);
        if (prev == null) {
            return getZeroValue();
        }
        return prev;
    }

    @Override
    public boolean containsValue(Object value) {
        return super.containsValue(value) ||
                (super.keySet().size() < size && value.equals(getZeroValue()));
    }
}
