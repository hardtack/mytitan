package kr.ac.cau.cse.team2.view.tree;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

public class ContainerTransferable implements Transferable {
    public static DataFlavor localObjectFlavor;

    static {
        try {
            localObjectFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DataFlavor[] supportedFlavors = {localObjectFlavor};
    Object object;

    public ContainerTransferable(Object o) {
        object = o;
    }

    public Object getTransferData(DataFlavor df)
            throws UnsupportedFlavorException {
        if (!isDataFlavorSupported(df)) {
            throw new UnsupportedFlavorException(df);
        }
        return object;
    }

    public boolean isDataFlavorSupported(DataFlavor df) {
        return (df.equals(localObjectFlavor));
    }

    public DataFlavor[] getTransferDataFlavors() {
        return supportedFlavors;
    }
}
