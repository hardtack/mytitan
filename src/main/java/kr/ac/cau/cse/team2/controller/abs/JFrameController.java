package kr.ac.cau.cse.team2.controller.abs;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Abstracted class for JFrame controller
 */
public abstract class JFrameController extends Controller<JFrame> implements WindowListener {
    private JMenuBar menuBar;

    @Override
    protected JFrame loadView() {
        JFrame view = new JFrame("My Titan");
        this.menuBar = new JMenuBar();
        view.setJMenuBar(this.menuBar);
        return view;
    }

    @Override
    protected void viewDidLoad() {
        this.getView().addWindowListener(this);
    }

    @Override
    protected void presentView() {
        this.getView().setVisible(true);
    }

    @Override
    protected void dismissView() {
        this.getView().setVisible(false);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.dismiss();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public void setMenuBar(JMenuBar menuBar) {
        this.menuBar = menuBar;
    }
}
