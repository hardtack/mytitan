package kr.ac.cau.cse.team2.util.function;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * High order functions.
 * Use guava's functions
 */
final public class HighOrder {
    private HighOrder() {
    }

    private static <F, T> void map(Iterable<F> source, Function<F, T> function, List<T> list) {
        for (F from : source) {
            list.add(function.apply(from));
        }
    }

    /**
     * Apply function to iterable and return list of result
     * Guava's transform function applies lazily.
     * So I prefer it.
     *
     * @param source   iterable source object
     * @param function a function
     * @param <F>      function argument type
     * @param <T>      function return type
     * @return list of applying result
     */
    public static <F, T> List<T> map(Iterable<F> source, Function<F, T> function) {
        List<T> list = Lists.newLinkedList();
        map(source, function, list);
        return list;
    }

    /**
     * Apply function to collection and return list of result
     * Guava's transform function applies lazily.
     * So I prefer it.
     *
     * @param source   a collection
     * @param function a function
     * @param <F>      function argument type
     * @param <T>      function return type
     * @return list of applying result
     */
    public static <F, T> List<T> map(Collection<F> source, Function<F, T> function) {
        List<T> list = new ArrayList<T>(source.size());
        map(source, function, list);
        return list;
    }

    /**
     * Replacement of foreach statement with index counting.
     *
     * @param iterable a iterable
     * @param forEach  a foreach statement function
     * @param <T>      type of iterable
     */
    public static <T> void foreach(Iterable<T> iterable, ForEach<T> forEach) {
        int index = 0;
        for (T item : iterable) {
            forEach.apply(index++, item);
        }
    }

    /**
     * Replacement of foreach statement with index counting.
     * returns list of result.
     *
     * @param iterable a iterable
     * @param forEach  a foreach statement function
     * @param <F>      type of iterable
     * @param <T>      conversion type
     * @return list of results
     */
    public static <F, T> List<T> foreachMap(Iterable<F> iterable, ForEachMap<F, T> forEach) {
        int index = 0;
        List<T> list = Lists.newLinkedList();
        for (F item : iterable) {
            list.add(forEach.apply(index++, item));
        }
        return list;
    }

    /**
     * Cartesian product with index
     *
     * @param iterable1 first iterable
     * @param iterable2 second iterable
     * @param function  a function
     * @param <T1>      a type
     * @param <T2>      a type
     */
    public static <T1, T2> void product(Iterable<T1> iterable1, final Iterable<T2> iterable2, final Product<T1, T2> function) {
        foreach(iterable1, new ForEach<T1>() {
            @Override
            public void apply(final int i, final T1 v1) {
                foreach(iterable2, new ForEach<T2>() {
                    @Override
                    public void apply(int j, T2 v2) {
                        function.apply(i, v1, j, v2);
                    }
                });
            }
        });
    }

    /**
     * Function for foreach.
     *
     * @param <T> a input type
     */
    public static interface ForEach<T> {
        public void apply(int i, T input);
    }

    /**
     * Function for foreach map
     *
     * @param <F> from type
     * @param <T> to type
     */
    public static interface ForEachMap<F, T> {
        public T apply(int i, F input);
    }

    /**
     * Function for cartesian product with index
     *
     * @param <T1> a input type
     * @param <T2> a input type
     */
    public static interface Product<T1, T2> {
        public void apply(int index1, T1 input1, int index2, T2 input2);
    }
}
