package kr.ac.cau.cse.team2.util.abs;

/**
 * Abstracted immutable map
 */
public abstract class AbstractImmutableMap<K, V> extends AbstractMap<K, V> {
    @Override
    public V put(K key, V value) {
        throw new UnsupportedOperationException("This is an immutable map.");
    }

    @Override
    public V remove(Object key) {
        throw new UnsupportedOperationException("This is an immutable map.");
    }
}
