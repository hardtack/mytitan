package kr.ac.cau.cse.team2.view;

import kr.ac.cau.cse.team2.view.event.TreeMutationListener;
import kr.ac.cau.cse.team2.view.tree.ContainerTransferable;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

/**
 * Drag and drop(a.k.a. DnD) enabled JTree
 */
public class DnDJTree extends JTree implements DragSourceListener, DropTargetListener, DragGestureListener {
    private DragSource dragSource;
    private DropTarget dropTarget;
    private DragGestureRecognizer dragGestureRecognizer;

    private Object dropTargetObject = null;
    private Object draggedObject = null;

    private List<TreeMutationListener> treeMutationListeners;

    public DnDJTree() {
        super();
        init();
    }

    public DnDJTree(TreeModel newModel) {
        super(newModel);
        init();
    }

    public DnDJTree(TreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
        init();
    }

    public DnDJTree(TreeNode root) {
        super(root);
        init();
    }

    public DnDJTree(Hashtable<?, ?> value) {
        super(value);
        init();
    }

    public DnDJTree(Vector<?> value) {
        super(value);
        init();
    }

    public DnDJTree(Object[] value) {
        super(value);
        init();
    }

    private void init() {
        this.treeMutationListeners = new ArrayList<TreeMutationListener>();
        this.dragSource = new DragSource();
        this.dragGestureRecognizer = dragSource.createDefaultDragGestureRecognizer(
                this,
                DnDConstants.ACTION_MOVE,
                this
        );
        this.dropTarget = new DropTarget(this, this);
    }

    public void addTreeMutationListener(TreeMutationListener listener) {
        this.treeMutationListeners.add(listener);
    }

    public void removeTreeMutationListener(TreeMutationListener listener) {
        this.treeMutationListeners.remove(listener);
    }

    public void removeTreeMutationListener(int idx) {
        this.treeMutationListeners.remove(idx);
    }

    // DragGestureListener
    public void dragGestureRecognized(DragGestureEvent e) {
        // find object at this x,y
        Point clickPoint = e.getDragOrigin();
        TreePath path = getPathForLocation(clickPoint.x, clickPoint.y);
        if (path == null) {
            return;
        }
        draggedObject = path.getLastPathComponent();
        Transferable trans = new ContainerTransferable(draggedObject);
        dragSource.startDrag(e, Cursor.getDefaultCursor(), trans, this);
    }

    // DragSourceListener events
    public void dragDropEnd(DragSourceDropEvent e) {
        dropTargetObject = null;
        draggedObject = null;
        updateUI();
    }

    public void dragEnter(DragSourceDragEvent e) {
    }

    public void dragExit(DragSourceEvent e) {
    }

    public void dragOver(DragSourceDragEvent e) {
    }

    public void dropActionChanged(DragSourceDragEvent e) {
    }

    // DropTargetListener events
    public void dragEnter(DropTargetDragEvent e) {
        e.acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE);
    }

    public void dragExit(DropTargetEvent e) {
    }

    public void dragOver(DropTargetDragEvent e) {
        // figure out which cell it's over, no drag to self
        Point dragPoint = e.getLocation();
        TreePath path = getPathForLocation(dragPoint.x, dragPoint.y);
        if (path == null) {
            dropTargetObject = null;
        } else {
            dropTargetObject = path.getLastPathComponent();
        }
        updateUI();
    }

    public void drop(DropTargetDropEvent e) {
        Point dropPoint = e.getLocation();
        TreePath path = getPathForLocation(dropPoint.x, dropPoint.y);

        if (path == null) {
            return;
        }

        e.acceptDrop(DnDConstants.ACTION_MOVE);

        DefaultMutableTreeNode droppedObject = null;
        try {
            droppedObject = (DefaultMutableTreeNode) e.getTransferable().getTransferData(ContainerTransferable.localObjectFlavor);
        } catch (UnsupportedFlavorException ignored) {
        } catch (IOException ignored) {
        }
        if (droppedObject == null) {
            return;
        }

        DefaultMutableTreeNode dropObject = (DefaultMutableTreeNode) path.getLastPathComponent();
        if (dropObject == droppedObject) {
            return;
        }
        // insert into spec'd path. if dropped into a parent
        // make it last child of that parent
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) droppedObject.getParent();


        // Trigger event
        for (TreeMutationListener listener : treeMutationListeners) {
            listener.willUpdate(this);
        }
        if ((parent != getModel().getRoot() && parent == dropObject) ||
                !dropObject.getAllowsChildren()) {
            DefaultMutableTreeNode dropParent = (DefaultMutableTreeNode) dropObject.getParent();
            int index = dropParent.getIndex(dropObject);

            for (TreeMutationListener listener : treeMutationListeners) {
                listener.willRemoveChild(parent, droppedObject);
            }
            parent.remove(droppedObject);
            for (TreeMutationListener listener : treeMutationListeners) {
                listener.didRemoveChild(parent, droppedObject);
            }

            for (TreeMutationListener listener : treeMutationListeners) {
                listener.willAddChild(dropParent, droppedObject, index);
            }
            dropParent.insert(droppedObject, index);
            for (TreeMutationListener listener : treeMutationListeners) {
                listener.didAddChild(dropParent, droppedObject, index);
            }
        } else {
            for (TreeMutationListener listener : treeMutationListeners) {
                listener.willRemoveChild(parent, droppedObject);
            }
            parent.remove(droppedObject);
            for (TreeMutationListener listener : treeMutationListeners) {
                listener.didRemoveChild(parent, droppedObject);
            }
            int index = dropObject.getChildCount();

            for (TreeMutationListener listener : treeMutationListeners) {
                listener.willAddChild(dropObject, droppedObject, index);
            }
            dropObject.add(droppedObject);
            for (TreeMutationListener listener : treeMutationListeners) {
                listener.didAddChild(dropObject, droppedObject, index);
            }
        }
        e.dropComplete(true);

        // Trigger event
        for (TreeMutationListener listener : treeMutationListeners) {
            listener.didUpdate(this);
        }
    }

    public void dropActionChanged(DropTargetDragEvent e) {
    }

    public Object getDropTargetObject() {
        return dropTargetObject;
    }

    public boolean isDropTargetLeaf() {
        Object object = this.getDropTargetObject();
        return object == null || this.getModel().isLeaf(object);
    }
}
