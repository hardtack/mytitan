package kr.ac.cau.cse.team2.view.event;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Event listener for tree mutation.
 */
public interface TreeMutationListener {
    public void willUpdate(JTree tree);
    public void didUpdate(JTree tree);

    public void willRemoveChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child);
    public void didRemoveChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child);

    public void willAddChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child, int index);
    public void didAddChild(DefaultMutableTreeNode parent, DefaultMutableTreeNode child, int index);
}
