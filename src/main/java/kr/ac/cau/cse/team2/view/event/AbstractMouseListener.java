package kr.ac.cau.cse.team2.view.event;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Abstracted MouseLister class for shorter code
 */
public abstract class AbstractMouseListener implements MouseListener {
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
