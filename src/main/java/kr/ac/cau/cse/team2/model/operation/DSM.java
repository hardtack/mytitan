package kr.ac.cau.cse.team2.model.operation;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;
import kr.ac.cau.cse.team2.util.SparseMatrix;
import kr.ac.cau.cse.team2.util.function.HighOrder;

import java.util.Arrays;
import java.util.List;

/**
 * Create DSM with list of modules.
 */
public final class DSM {
    private DSM() {
    }

    /**
     * Create DSM from array of modules.
     *
     * @param modules module array
     * @return a DSM
     */
    public static Table<Integer, Integer, Boolean> createDSM(Module... modules) {
        return createDSM(Arrays.asList(modules));
    }

    /**
     * Create DSM from list of modules.
     *
     * @param modules module list
     * @return a DSM
     */
    public static Table<Integer, Integer, Boolean> createDSM(List<Module> modules) {
        final List<Module> rowColumnList = Lists.newArrayList(Iterables.concat(Lists.transform(modules, new Function<Module, Iterable<Module>>() {
            @Override
            public Iterable<Module> apply(Module input) {
                return ModuleTraversable.traverse(input);
            }
        })));
        int size = rowColumnList.size();
        final SparseMatrix<Boolean> matrix = new SparseMatrix<Boolean>(size, size, false);
        HighOrder.product(rowColumnList, rowColumnList, new HighOrder.Product<Module, Module>() {
            @Override
            public void apply(int i, Module from, int j, Module to) {
                if (i != j) {
                    if (from.hasDependencyOn(to)) {
                        matrix.put(i, j, true);
                    }
                }
            }
        });
        return matrix;
    }

    public static List<Module> createModules(Table<Integer, Integer, Boolean> matrix, List<String> names) {
        List<Module> modules = Lists.newArrayList(FluentIterable.from(names).transform(new Function<String, Module>() {
            @Override
            public Module apply(String name) {
                return new Module(name);
            }
        }));
        for (Table.Cell<Integer, Integer, Boolean> cell : matrix.cellSet()) {
            boolean value = cell.getValue();
            if (value) {
                modules.get(cell.getRowKey()).addDependency(modules.get(cell.getColumnKey()));
            }
        }

        return modules;
    }

    /**
     * Collect and return all leaf module (not grouped module)
     * @param root root grouped module
     * @return list of modules
     */
    public static List<Module> leafModules(GroupedModule root) {
        List<Module> modules = Lists.newLinkedList();
        for (Module subModule:root.getSubModules()){
            if (subModule instanceof GroupedModule) {
                modules.addAll(leafModules((GroupedModule) subModule));
            } else {
                modules.add(subModule);
            }
        }
        return modules;
    }

    /**
     * Find module at index of matrix
     *
     * @param index a index
     * @param root  a root module
     * @return found module if exists, or null
     */
    public static Module moduleAtIndex(int index, GroupedModule root) {
        int currentIndex = 0;
        for (Module subModule : ModuleTraversable.traverse(root)) {
            if (currentIndex == index) {
                return subModule;
            }
            currentIndex++;
        }
        return null;
    }

    /**
     * Find index of module in matrix.
     *
     * @param module a module
     * @param root   a root module
     * @return index of module. returns -1 if not found.
     */
    public static int indexOfModule(Module module, GroupedModule root) {
        int index = 0;
        for (Module subModule : ModuleTraversable.traverse(root)) {
            if (subModule == module) {
                return index;
            }
            index++;
        }
        return -1;
    }

    /**
     * Calculated size of DSM.
     *
     * @param root a root module
     * @return size of DSM
     */
    public static int dsmSize(GroupedModule root) {
        int count = 0;
        for (Module ignored : ModuleTraversable.traverse(root)) {
            count++;
        }
        return count;
    }
}
