package kr.ac.cau.cse.team2.model.module;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import kr.ac.cau.cse.team2.model.abs.Model;

import java.util.HashSet;
import java.util.Set;

/**
 * Module class
 * Describes module and its dependencies
 */
public class Module extends Model {

    private String name;
    private Set<Module> dependencies;

    public Module(String name) {
        this.setName(name);
        this.dependencies = new HashSet<Module>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Module> getDependencies() {
        return Sets.newHashSet(this.dependencies);
    }

    public void addDependency(Module module) {
        if (module instanceof GroupedModule) {
            throw new UnsupportedOperationException("Cannot have dependencies to group directly");
        }
        this.dependencies.add(module);
    }

    public void removeDependency(Module module) {
        if (module instanceof GroupedModule) {
            for (Module subModule : ((GroupedModule) module).getSubModules()) {
                this.removeDependency(subModule);
            }
        } else {
            if (this.dependencies.contains(module)) {
                this.dependencies.remove(module);
            }
        }
    }

    public boolean hasDependencyOn(Module module) {
        if (module instanceof GroupedModule) {
            return Iterables.any(((GroupedModule) module).getSubModules(), new Predicate<Module>() {
                @Override
                public boolean apply(Module subModule) {
                    return hasDependencyOn(subModule);
                }
            });
        }
        return this.dependencies.contains(module);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GroupedModule) {
            return false;
        }
        if (obj instanceof Module) {
            Module module = (Module) obj;
            return this.name.equals(module.name) && this.dependencies.equals(module.dependencies);
        }
        return false;
    }
}
