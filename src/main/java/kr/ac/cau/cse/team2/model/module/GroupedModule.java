package kr.ac.cau.cse.team2.model.module;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class that representing grouped module.
 */
public class GroupedModule extends Module {
    private List<Module> subModules;
    private boolean open;

    /**
     * @param name name of module
     */
    public GroupedModule(String name) {
        this(name, false);
    }

    /**
     * @param name name of module
     * @param open is opened? (default: false)
     */
    public GroupedModule(String name, boolean open) {
        this(name, new ArrayList<Module>(), open);
    }

    /**
     * @param name       name of module
     * @param subModules sub modules
     */
    public GroupedModule(String name, Module... subModules) {
        this(name, subModules, false);
    }

    /**
     * @param name       name of module
     * @param subModules sub modules
     * @param open       is opened? (default: false)
     */
    public GroupedModule(String name, Module[] subModules, boolean open) {
        this(name, Arrays.asList(subModules), open);
    }


    /**
     * @param name       name of module
     * @param subModules sub modules
     */
    public GroupedModule(String name, List<Module> subModules) {
        this(name, subModules, false);
    }

    /**
     * @param name       name of module
     * @param subModules sub modules
     * @param open       is opened? (default: false)
     */
    public GroupedModule(String name, List<Module> subModules, boolean open) {
        super(name);
        this.open = open;
        this.subModules = subModules;
    }

    /**
     * @return module open state
     */
    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    /**
     * @return list of sub modules
     */
    public List<Module> getSubModules() {
        return subModules;
    }

    public void setSubModules(List<Module> subModules) {
        this.subModules = subModules;
    }

    /**
     * Append sub module to sub module list
     *
     * @param module a module
     */
    public void addSubModule(Module module) {
        this.subModules.add(module);
    }

    /**
     * Add sub module to sub module list
     *
     * @param index  at index
     * @param module a module
     */
    public void addSubModule(int index, Module module) {
        this.subModules.add(index, module);
    }

    /**
     * Remove sub module from sub module list
     *
     * @param index at index
     * @return the module previously at the specified position
     */
    public Module removeSubModule(int index) {
        return this.subModules.remove(index);
    }

    /**
     * Remove sub module from sub module list
     *
     * @param module a module
     * @return <tt>true</tt> if the sub module list contained the specified element
     */
    public boolean removeSubModule(Module module) {
        return this.subModules.remove(module);
    }

    /**
     * Indicate that the group containing the module
     *
     * @param module a module
     * @return containing state
     */
    public boolean containsModule(final Module module) {
        return Iterables.any(
                this.subModules,
                Predicates.or(
                        Predicates.equalTo(module),
                        Predicates.and(
                                Predicates.instanceOf(GroupedModule.class),
                                new Predicate<Module>() {
                                    @Override
                                    public boolean apply(Module group) {
                                        return ((GroupedModule) group).containsModule(module);
                                    }
                                }
                        )
                )
        );
    }

    @Override
    public boolean hasDependencyOn(final Module module) {
        return Iterables.any(this.subModules, new Predicate<Module>() {
            @Override
            public boolean apply(Module subModule) {
                return subModule.hasDependencyOn(module);
            }
        });
    }

    @Override
    public void addDependency(Module module) {
        throw new UnsupportedOperationException("Group cannot have dependencies directly");
    }

    @Override
    public void removeDependency(Module module) {
        for (Module subModule : subModules) {
            subModule.removeDependency(module);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GroupedModule) {
            GroupedModule group = (GroupedModule) obj;
            return this.getName().equals(group.getName()) && this.subModules.equals(group.subModules);
        }
        if (obj instanceof Module) {
            return false;
        }
        return false;
    }
}
