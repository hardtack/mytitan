package kr.ac.cau.cse.team2.util;

/**
 * Simple two-values pair
 */
public class Pair<L, R> {
    private final L left;
    private final R right;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public static <L, R> Pair<L, R> create(L left, R right) {
        return new Pair<L, R>(left, right);
    }

    public L getLeft() {
        return left;
    }

    public R getRight() {
        return right;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pair) {
            Pair pair = (Pair) obj;
            return this.left.equals(pair.left) && this.right.equals(pair.right);
        }
        return super.equals(obj);
    }
}
