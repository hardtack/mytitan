package kr.ac.cau.cse.team2.util;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Range;
import com.google.common.collect.Table;
import kr.ac.cau.cse.team2.util.abs.AbstractImmutableMap;
import kr.ac.cau.cse.team2.util.abs.AbstractMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Sparse matrix with table interface
 */
public class SparseMatrix<V> implements Table<Integer, Integer, V> {
    private final SparseMap<V> emptyRow;
    private int rowSize;
    private int columnSize;
    private SparseMap<SparseMap<V>> rows;
    private V zeroValue;

    public SparseMatrix(int rowSize, int columnSize, V zeroValue) {
        this.rowSize = rowSize;
        this.columnSize = columnSize;
        this.zeroValue = zeroValue;
        this.emptyRow = new SparseMap<V>(columnSize, zeroValue);

        this.rows = new SparseMap<SparseMap<V>>(this.rowSize, emptyRow);
    }

    public int columnSize() {
        return columnSize;
    }

    public int rowSize() {
        return rowSize;
    }

    public V getZeroValue() {
        return zeroValue;
    }

    @Override
    public boolean contains(Object rowKey, Object columnKey) {
        return containsRow(rowKey) && containsColumn(columnKey);
    }

    @Override
    public boolean containsRow(Object rowKey) {
        if (!(rowKey instanceof Integer)) {
            return false;
        }
        Integer rowIndex = (Integer) rowKey;
        return this.rowSize > rowIndex;
    }

    @Override
    public boolean containsColumn(Object columnKey) {
        if (!(columnKey instanceof Integer)) {
            return false;
        }

        Integer columnIndex = (Integer) columnKey;
        return this.columnSize > columnIndex;
    }

    @Override
    public boolean containsValue(Object value) {
        for (SparseMap<V> row : this.rows.values()) {
            if (row.containsValue(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object rowKey, Object columnKey) {
        return this.rows.get(rowKey).get(columnKey);
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return rowSize * columnSize;
    }

    @Override
    public void clear() {
        for (Integer row : rowKeySet()) {
            this.rows.put(row, emptyRow);
        }
    }

    @Override
    public V put(Integer rowKey, Integer columnKey, V value) {
        SparseMap<V> row = this.rows.get(rowKey);
        if (row == emptyRow) {
            row = new SparseMap<V>(this.columnSize, zeroValue);
            this.rows.put(rowKey, row);
        }
        return row.put(columnKey, value);
    }

    @Override
    public void putAll(Table<? extends Integer, ? extends Integer, ? extends V> table) {
        for (Cell<? extends Integer, ? extends Integer, ? extends V> cell : table.cellSet()) {
            this.put(cell.getRowKey(), cell.getColumnKey(), cell.getValue());
        }
    }

    @Override
    public V remove(Object rowKey, Object columnKey) {
        return this.rows.get(rowKey).remove(columnKey);
    }

    @Override
    public Map<Integer, V> row(Integer rowKey) {
        return this.rows.get(rowKey);
    }

    @Override
    public Map<Integer, V> column(final Integer columnKey) {
        return new AbstractImmutableMap<Integer, V>() {
            @Override
            public int size() {
                return rowSize;
            }

            @Override
            public V get(Object key) {
                return rows.get(key).get(columnKey);
            }

            @Override
            public Set<Integer> keySet() {
                return columnKeySet();
            }
        };
    }

    @Override
    public Set<Cell<Integer, Integer, V>> cellSet() {
        return FluentIterable.from(
                Iterables.concat(
                        Collections2.transform(
                                this.rows.entrySet(),
                                new Function<Map.Entry<Integer, SparseMap<V>>, Iterable<Cell<Integer, Integer, V>>>() {
                                    @Override
                                    public Iterable<Cell<Integer, Integer, V>> apply(Map.Entry<Integer, SparseMap<V>> input) {
                                        final int rowIndex = input.getKey();
                                        final SparseMap<V> row = input.getValue();
                                        return Iterables.transform(row.entrySet(), new Function<Map.Entry<Integer, V>, Cell<Integer, Integer, V>>() {
                                            @Override
                                            public Cell<Integer, Integer, V> apply(final Map.Entry<Integer, V> input) {
                                                return new Cell<Integer, Integer, V>() {
                                                    @Override
                                                    public Integer getRowKey() {
                                                        return rowIndex;
                                                    }

                                                    @Override
                                                    public Integer getColumnKey() {
                                                        return input.getKey();
                                                    }

                                                    @Override
                                                    public V getValue() {
                                                        return input.getValue();
                                                    }
                                                };
                                            }
                                        });
                                    }
                                }
                        )
                )
        ).toSet();
    }

    @Override
    public Set<Integer> rowKeySet() {
        return this.rows.keySet();
    }

    @Override
    public Set<Integer> columnKeySet() {
        return ContiguousSet.create(Range.closedOpen(0, columnSize), DiscreteDomain.integers());
    }

    @Override
    public Collection<V> values() {
        return Collections2.transform(cellSet(), new Function<Cell<Integer, Integer, V>, V>() {
            @Override
            public V apply(Cell<Integer, Integer, V> input) {
                return input.getValue();
            }
        });
    }

    @Override
    public Map<Integer, Map<Integer, V>> rowMap() {
        return new AbstractMap<Integer, Map<Integer, V>>() {
            @Override
            public Map<Integer, V> get(Object key) {
                return rows.get(key);
            }

            @Override
            public Map<Integer, V> put(Integer key, Map<Integer, V> value) {
                SparseMap<V> row = new SparseMap<V>(columnSize, zeroValue);
                row.putAll(value);
                return rows.put(key, row);
            }

            @Override
            public Map<Integer, V> remove(Object key) {
                return rows.remove(key);
            }

            @Override
            public Set<Integer> keySet() {
                return rows.keySet();
            }
        };
    }

    @Override
    public Map<Integer, Map<Integer, V>> columnMap() {
        return new AbstractMap<Integer, Map<Integer, V>>() {
            @Override
            public Map<Integer, V> get(final Object rowKey) {
                return new AbstractMap<Integer, V>() {

                    @Override
                    public V get(Object key) {
                        return rows.get(rowKey).get(key);
                    }

                    @Override
                    public V put(Integer key, V value) {
                        return rows.get(rowKey).put(key, value);
                    }

                    @Override
                    public V remove(Object key) {
                        return rows.get(rowKey).remove(key);
                    }

                    @Override
                    public Set<Integer> keySet() {
                        return rowKeySet();
                    }
                };
            }

            @Override
            public Map<Integer, V> put(Integer rowKey, Map<Integer, V> value) {
                Map<Integer, V> prev = new SparseMap<V>(rowSize, zeroValue);
                for (Map.Entry<Integer, V> entry : value.entrySet()) {
                    prev.put(entry.getKey(), rows.get(rowKey).put(entry.getKey(), entry.getValue()));
                }
                return prev;
            }

            @Override
            public Map<Integer, V> remove(Object key) {
                Map<Integer, V> prev = new SparseMap<V>(rowSize, zeroValue);
                for (Map.Entry<Integer, SparseMap<V>> entry : rows.entrySet()) {
                    prev.put(entry.getKey(), entry.getValue().remove(key));
                }
                return prev;
            }

            @Override
            public Set<Integer> keySet() {
                return columnKeySet();
            }
        };
    }

}
