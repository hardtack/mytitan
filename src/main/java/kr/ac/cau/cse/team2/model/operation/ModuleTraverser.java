package kr.ac.cau.cse.team2.model.operation;

import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Traverse module for DSM
 */
public class ModuleTraverser implements Iterator<Module> {
    private Module target;
    private ModuleTraverser currentNestedTraverser;
    private Queue<Module> queue;

    public ModuleTraverser(Module module) {
        this.target = module;
        if (this.target instanceof GroupedModule && this.group().isOpen()) {
            this.queue = new LinkedList<Module>(this.group().getSubModules());
        } else {
            this.queue = new LinkedList<Module>();
            this.queue.add(this.target);
        }
    }

    private GroupedModule group() {
        return (GroupedModule) this.target;
    }

    @Override
    public boolean hasNext() {
        if (!this.queue.isEmpty() ||
                (this.currentNestedTraverser != null && this.currentNestedTraverser.hasNext())) {
            return true;
        }
        this.currentNestedTraverser = null;
        return false;
    }

    @Override
    public Module next() {
        if (this.currentNestedTraverser != null && this.currentNestedTraverser.hasNext()) {
            return this.currentNestedTraverser.next();
        }
        this.currentNestedTraverser = null;
        Module next = this.queue.remove();
        if (this.target instanceof GroupedModule && this.group().isOpen()) {
            this.currentNestedTraverser = new ModuleTraverser(next);
            return this.next();
        }
        return next;
    }
}
