package kr.ac.cau.cse.team2.util.io;

import com.google.common.collect.Table;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Writes DSM file format.
 */
public class DSMWriter {
    public void writeDSM(Table<Integer, Integer, Boolean> matrix, Iterable<String> names, OutputStream stream) throws IOException {
        Writer writer = new OutputStreamWriter(stream);
        int size = matrix.rowKeySet().size();
        writer.write(size + "\n");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                writer.write((matrix.get(i, j) ? "1" : "0") + "\n");
            }
            writer.write("\n");
        }
        for (String name : names) {
            writer.write(name);
            writer.write('\n');
        }
        writer.flush();
    }

    public void writeDSM(Table<Integer, Integer, Boolean> matrix, Iterable<String> names, File file) throws IOException {
        FileOutputStream stream = new FileOutputStream(file);
        try {
            writeDSM(matrix, names, stream);
        } finally {
            stream.close();
        }
    }

    public void writeDSM(Table<Integer, Integer, Boolean> matrix, Iterable<String> names, String filename) throws IOException {
        writeDSM(matrix, names, new File(filename));
    }
}
