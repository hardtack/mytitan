package kr.ac.cau.cse.team2.model.operation;

import kr.ac.cau.cse.team2.model.module.Module;

import java.util.Iterator;

/**
 * Iterable implementation for module traverser.
 */
public class ModuleTraversable implements Iterable<Module> {
    private Module target;

    public ModuleTraversable(Module target) {
        this.target = target;
    }

    public static ModuleTraversable traverse(Module target) {
        return new ModuleTraversable(target);
    }

    @Override
    public Iterator<Module> iterator() {
        return new ModuleTraverser(this.target);
    }
}
