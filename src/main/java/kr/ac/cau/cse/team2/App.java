package kr.ac.cau.cse.team2;

import kr.ac.cau.cse.team2.controller.MainController;
import kr.ac.cau.cse.team2.controller.abs.Controller;

import javax.swing.JFrame;

/**
 * The application
 */
public class App implements Controller.ControllerListener<JFrame> {
    Controller<?> rootController;
    MainController controller;

    /**
     * Start the application
     */
    public void main() {
        this.controller = new MainController();
        this.controller.addListener(this);

        this.rootController = this.controller;
        this.rootController.present();
    }

    @Override
    public void controllerWillPresent(Controller<JFrame> controller) {

    }

    @Override
    public void controllerDidPresent(Controller<JFrame> controller) {

    }

    @Override
    public void controllerWillDismiss(Controller<JFrame> controller) {

    }

    @Override
    public void controllerDidDismiss(Controller<JFrame> controller) {
        System.exit(0);
    }
}
