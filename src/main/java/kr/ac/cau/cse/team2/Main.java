package kr.ac.cau.cse.team2;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Execution point of application
 */
public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    String name = System.getProperty("os.name").toLowerCase();
                    if (name.contains("mac")) {  // Mac OS X support
                        System.setProperty("apple.laf.useScreenMenuBar", "true");
                        System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Test");
                    }
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    new App().main();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
