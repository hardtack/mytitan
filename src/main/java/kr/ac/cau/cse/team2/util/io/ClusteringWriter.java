package kr.ac.cau.cse.team2.util.io;

import com.google.common.base.Strings;
import com.google.common.xml.XmlEscapers;
import kr.ac.cau.cse.team2.model.module.GroupedModule;
import kr.ac.cau.cse.team2.model.module.Module;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

/**
 * Writes clustering file format.
 */
public class ClusteringWriter {
    private void writeClustering(List<Module> modules, Writer writer, int indent) throws IOException {
        for (Module module : modules) {
            writer.write(Strings.repeat(" ", indent));
            if (module instanceof GroupedModule) {
                writer.write("<group name=\"" + XmlEscapers.xmlAttributeEscaper().escape(module.getName()) + "\">\n");
                writeClustering(((GroupedModule) module).getSubModules(), writer, indent + 2);
                writer.write(Strings.repeat(" ", indent));
                writer.write("</group>\n");
            } else {
                writer.write("<item name=\"" + XmlEscapers.xmlAttributeEscaper().escape(module.getName()) + "\" />\n");
            }
        }
    }

    public void writeClustering(List<Module> modules, OutputStream stream) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(stream));
        writer.write("<cluster>\n");
        writeClustering(modules, writer, 2);
        writer.write("</cluster>");
        writer.flush();
    }

    public void writeClustering(Module module, File file) throws IOException {
        writeClustering(Arrays.asList(module), file);
    }

    private void writeClustering(List<Module> modules, File file) throws IOException {
        FileOutputStream stream = new FileOutputStream(file);
        try {
            writeClustering(modules, stream);
        } finally {
            stream.close();
        }
    }
}
